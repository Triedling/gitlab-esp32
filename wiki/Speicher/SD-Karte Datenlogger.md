#  SD-Karte Datenlogger 

###  Zielsetzung 
* Verbinden von einem SD-Karten-Modul über die SPI Schnittstelle mit dem ESP32.
* Messdaten in einem Array speichern und in einen String umwandeln
* Formatierung des Strings, dass er in ein Tabellenkalkulationsprogramm importiert werden kann
* Speichern der Messdaten in einer Textdatei auf der SD-Karte

###  benötigte Libraries 

* SD für dei SD-Karte [Dokumentation](https://www.arduino.cc/reference/en/libraries/sd/)  
* SPI Für die Kommunikation über die SPI-Schnittstelle

###  Verkabelung 
![[esp32-sd_card_steckplatine.svg]]

|MicroSD card module | ESP32 |
|---|---|
|GND	|GND|
|VCC	|5V (3.3V)|
|MISO	|GPIO 19|
|MOSI	|GPIO 23|
|CLK	|GPIO 18|
|CS	|GPIO 5|

In den Anleitungen steht, dass man den Adapter mit 3,3V verbinden soll, da die SD Karte mit 3,3V läuft. Falls man einen Adapter mit Spannungsregelung hat, kann es sein, dass man VCC an 5V anschließen muss, damit die Verbindung klappt. 
Bei unserem Modul kam mit 3,3V der Fehler Initialisierung fehlgeschlagen. 

### Zu Beachten
* Bei SDHC-Karten laufen die Programme instabil und es kommt immer wieder zu Lesefehlern. Deshalb nutzen wir SD-Karten bis maximal 2GB.
* Die SD-Karte muss  in FAT32 formatiert sein. Die Formatierungstools in Betriebssystem sind nicht für SD-Karten optimiert. Es kann folgende Freeware verwendet werden. [SD Card Formatter](https://www.sdcard.org/downloads/formatter/)
* Die Textdatei in die die Daten abgespeichert werden soll, muss zuvor händisch im Stammverzeichnis der SD-Karte angelegt werden. Es müssen Großbuchstaben verwendet werden (z. B. "LSZU.TXT"). 

###  Programmierung 

```c
/* Art ino: SD-Karte - Messwerte in String umwandeln und speichern
   Bauteil: SD-Karten-Modul
   VCC:       5 V
   GND:       GND
   SPI-MISO:  19
   SPI-MOSI:  23
   SCK(clk):  18
   CS:        5

   Bearbeitet: Matthias Weis, 04.2022 auf Basis von Volker Rust, 01.2020

   In diesem Programm ist eigentlich nur die Funktion "speichern()" 
   wichtig. Diese sollte man ansehen und verstehen.

   Kann die SD-Karte nicht initialisiert werden, sollte sie enfernt 
   und nochmal eingeführt werden.

   Die SD-Karte hat ihre Tücken. Damit sie die Daten sauber abspeichert werden sollte
   das Programm auf dne Microcontroller geladen werden. Dann erst die SD-Karte eingefhrt
   und anschließend ein Reset des Mikrocontrollers erfolgen.

*/

#include <SPI.h>    //Bibliothek für die Kommnikation mit der SD-Karte
#include <SD.h>     //Bibliothek für die SD-Karte

File Textdatei;
String dateiName = "/lszu.txt";   //Dateinamen angeben. Vor den Dateinamen muss ein / gesetzt werden
int messwerte[] = {0,1,2,3};     //Es wird ein Array für die Zeitmessung erzeugt. Perspektivisch sollte eine genauere Zeitmessung 
String dataString = "";           //Erzeuge leeren String zur Datenaufnahme

void setup() {
  Serial.begin(9600);
  dataString = dataString + "Messwert 1:; Messwert 2:; Messwert 3:; Zeit:
";    //Benennung der Tabellenspalten mit /n wird ein Zeilenumbruch eingefügt
  initSD();
}

void loop() {
  messwerte[3] = millis();
  messwerte[2] = messwerte [3]/4;        // Hier solltn Messwerte in das Array übertragen werden

  speichern();

  delay(1000);   // Gibt die Zeit zwischen der Entnahme zweier Messwerte an 

  }

void speichern() {

  //Fülle den DatenString mit Messwerten aus den Array in dem die Messwerte
  //gespeichert sind. Hierzu "addiert" man das nächste gewünschte Zeichen
  //an das Ende des Datenstrings an. Achtung: Es muss ein ZEICHEN sein und
  //keine Zahl, deshalb wandelt man Integer erst noch in einen String um.
  //Zum Trennen der Tabellenspalten für das Tabellen-Programm zur Auswertung
  //fügt man nach jedem Messwert ein Semikolon an.

  for (int i = 0; i <= 3; i++) {                    //Die Schleife wird durchlaufen, bis alle aufgenommen Messwerte in den String eingefügt wurden
    dataString = dataString + String(messwerte[i]); //Dazu wird die neue Zeile an den bisherigen String angefügt
    dataString = dataString + ";";                  //Um die Daten als CSV-Datei zu importieren werden sie durch ein Semikolon abgegrenzt

  }

 dataString = dataString + "
";                    // Abschließend erfolgt ein Zeilenumbruch

  Textdatei = SD.open(dateiName, FILE_WRITE);
  if (Textdatei) {
    Textdatei.println(dataString);
    Textdatei.close();
    Serial.println("Daten gepeichert!");
    delay(10);
    //Serial.print(dataString);
  }

  else {
    Serial.println("Fehler beim Öffnen der Datei");
    }

}

void initSD() {
  Serial.print("Initialisierung ...");
  if (!SD.begin(5)) {                          // Wenn die SD-Karte nicht (!SD.begin) gefunden werden kann, ...
    Serial.println(" fehlgeschlagen!");        // ... soll eine Fehlermeldung ausgegeben werden. ....
    //return;
    // don't do anything more:
    while (1);
  }
  Serial.println(" abgeschlossen");            // ... Ansonsten soll die Meldung "Initialisierung abgeschlossen." ausgegeben werden.

}
```

###  Beispiel Datenspeicherung beim BME280 

```c
/***************************************************************************
  Fortbildung Mikrocontroller LSZU

  Verbindung ESP32 - BME280
 ***************************************************************************/

#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>
#include <SPI.h>    //Bibliothek für die Kommnikation mit der SD-Karte
#include <SD.h>     //Bibliothek für die SD-Karte

#define SEALEVELPRESSURE_HPA (1013.25)

File Textdatei;     // An dieser Stelle wird die Variable "Textdatei" als File (dts. Datei) deklariert.
String dateiName = "/lszu.txt";   //Dateinamen angeben. Vor den Dateinamen muss ein / gesetzt werden
int messwerte[] = {0, 1, 2, 3};  //Es wird ein Array für die Zeitmessung erzeugt. Perspektivisch sollte eine genauere Zeitmessung
String dataString = "";           //Erzeuge leeren String zur Datenaufnahme

Adafruit_BME280 bme;

unsigned long delayTime;
int sda = 21;             // es können auch andere GPIO als SDA bzw. SCL verwendet werden
int scl = 22;             // die hier gewählten sind die bei unserem Mikrocontroller voreingestellten GPIOs
void setup() {

  Serial.begin(115200);
  while (!Serial);

  dataString = dataString + "Temperatur in °C:; Luftdruck in hPa:; Höhe über Normalnull:; Luftfeuchtigkeit in %
";    //Benennung der Tabellenspalten mit /n wird ein Zeilenumbruch eingefügt
  initSD();

  Serial.println("BME280");

  unsigned status;

  // default Einstellungen
  Wire.begin(sda, scl);       // hier wird die Verkabelung zum bme280 übergeben
  status = bme.begin(0x76);  // hier muss die Adresse stehen, die man mit I2C-Test herausgefunden hat entweder 0x76 oder 0x77

  if (!status) {
    Serial.println("Es ist kein passender BME280 sensor angeschlossen, stimmt die Verkabelung, die Adresse, Sensor ID!");
    Serial.print("SensorID was: 0x"); Serial.println(bme.sensorID(), 16);
    while (1) delay(10);
  }

  Serial.println("-- default Test --");
  delayTime = 1000;

  Serial.println();
}

void loop() {
  printValues();
  messwerte[0] = bme.readTemperature();
  messwerte[1] =bme.readPressure() / 100.0F;
  messwerte[2] =bme.readAltitude(SEALEVELPRESSURE_HPA);
  messwerte[3] =bme.readHumidity();
  speichern();

  delay(delayTime);
}

void printValues() {

  //Serial.print("SensorID ist: 0x"); Serial.println(bme.sensorID(),16);  // Die SensorID kann ausgegeben werden, um zu unterscheiden,
  Serial.print("Temperatur = ");                                          // welcher Sensor angeschlossen ist (BMP180, BMP085, BMP280, BME280, BME 680)
  Serial.print(bme.readTemperature());
  Serial.println(" *C");

  Serial.print("Luftdruck = ");
  Serial.print(bme.readPressure() / 100.0F); //Umrechnung von Pa in hPa. Das F stellt sicher, dass durch eine Gleitkommazahl geteilt wird
  Serial.println(" hPa");

  Serial.print("Höhe über Meeresspiegel = ");
  Serial.print(bme.readAltitude(SEALEVELPRESSURE_HPA));
  Serial.println(" m");

  Serial.print("Luftfeuchtigkeit = ");
  Serial.print(bme.readHumidity());
  Serial.println(" %");

  Serial.println();
}

void speichern() {

  //Fülle den DatenString mit Messwerten aus den Array in dem die Messwerte
  //gespeichert sind. Hierzu "addiert" man das nächste gewünschte Zeichen
  //an das Ende des Datenstrings an. Achtung: Es muss ein ZEICHEN sein und
  //keine Zahl, deshalb wandelt man Integer erst noch in einen String um.
  //Zum Trennen der Tabellenspalten für das Tabellen-Programm zur Auswertung
  //fügt man nach jedem Messwert ein Semikolon an.

  for (int i = 0; i <= 3; i++) {                    //Die Schleife wird durchlaufen, bis alle aufgenommen Messwerte in den String eingefügt wurden
    dataString = dataString + String(messwerte[i]); //Dazu wird die neue Zeile an den bisherigen String angefügt
    dataString = dataString + ";";                  //Um die Daten als CSV-Datei zu importieren werden sie durch ein Semikolon abgegrenzt

  }

  dataString = dataString + "
";                    // Abschließend erfolgt ein Zeilenumbruch

  Textdatei = SD.open(dateiName, FILE_WRITE);
  if (Textdatei) {
    Textdatei.println(dataString);
    Textdatei.close();
    Serial.println("Daten gepeichert!");
    delay(10);
    //Serial.print(dataString);
  }

  else {
    Serial.println("Fehler beim Öffnen der Datei");
  }

}

void initSD() {
  Serial.print("Initialisierung ...");
  if (!SD.begin(5)) {                          // Wenn die SD-Karte nicht (!SD.begin) gefunden werden kann, ...
    Serial.println(" fehlgeschlagen!");        // ... soll eine Fehlermeldung ausgegeben werden. ....
    while (1);
  }
  Serial.println(" abgeschlossen");            // ... Ansonsten soll die Meldung "Initialisierung abgeschlossen." ausgegeben werden.

}
```