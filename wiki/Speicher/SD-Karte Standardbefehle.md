###  Zielsetzung 
* Verbinden von einem SD-Karten-Modul über die SPI Schnittstelle mit dem ESP32.
* Daten vom ESP32 in einer Textdatei auf der SD-Karte speichern, die Textdatei um Daten erweitern und die Daten auslesen.

###  benötigte Libraries 

* SD für dei SD-Karte [Dokumentation](https://www.arduino.cc/reference/en/libraries/sd/)  
* SPI Für die Kommunikation über die SPI-Schnittstelle

###  Verkabelung 
![[esp32-sd_card_steckplatine.svg]]

|MicroSD card module | ESP32 |
|---|---|
|GND	|GND|
|VCC	|5V (3.3V)|
|MISO	|GPIO 19|
|MOSI	|GPIO 23|
|CLK	|GPIO 18|
|CS	|GPIO 5|

In den Anleitungen steht, dass man den Adapter mit 3,3V verbinden soll, da die SD Karte mit 3,3V läuft. Falls man einen Adapter mit Spannungsregelung hat, kann es sein, dass man VCC an 5V anschließen muss, damit die Verbindung klappt. 
Bei unserem Modul kam mit 3,3V der Fehler Card mount failed. 

### Zu Beachten
* Bei SDHC-Karten laufen die Programme instabil und es kommt immer wieder zu Lesefehlern. Deshalb nutzen wir SD-Karten bis maximal 2GB.
* Die SD-Karte muss  in FAT32 formatiert sein. Die Formatierungstools in Betriebssystem sind nicht für SD-Karten optimiert. Es kann folgende Freeware verwendet werden. [SD Card Formatter](https://www.sdcard.org/downloads/formatter/)
* Die Textdatei in die die Daten abgespeichert werden soll, muss zuvor händisch im Stammverzeichnis der SD-Karte angelegt werden. Es müssen Großbuchstaben verwendet werden (z. B. "LSZU.TXT"). 

###  Programmierung 

```c
/* 
   Bauteil: SD-Karten-Modul
   VCC:       5 V
   GND:       GND
   SPI-MISO:  19
   SPI-MOSI:  23
   SCK(clk):  18
   CS:        5

   Bearbeitet: Matthias Weis, 04.2022 auf Basis von Volker Rust, 01.2020

   In dem Programm gibt es Teile für schreiben und lesen und anfügen.
   Nicht alle diese Teile müssen in einem fertigen Projekt genutzt werden.

   ACHTUNG: Dieses Programm ist recht speziell und erzeugt
   manchmal beim Übertragen Fehler.
   Sollte das Übertragen nicht klappen, dann bitte ein zweites Mal versuchen.
   Sollte es bei der Arbeit mit der SD-Karte eine Fehlermeldung geben, hilf
   manchmal ein Reset des Arduinos.
   Kann die SD-Karte nicht initialisiert werden, sollte sie enfernt 
   und nochmal eingeführt werden.
*/

#include <SPI.h>    //Bibliothek für die Kommnikation mit der SD-Karte
#include <SD.h>     //Bibliothek für die SD-Karte

File Textdatei;     // An dieser Stelle wird die Variable "Textdatei" als File (dts. Datei) deklariert.

// diese Datei muss mit ausschließlich Großbuchstaben
// auf der SD-Karte schon vorhanden sein. ("DATEN.TXT").
// d.H. sie muss mit dem Computer auf der SD-Karte angelegt werden.
// Vor den Dateinamen muss ein / gesetzt werden
String dateiName = "/lszu.txt";

void setup() {
  Serial.begin(9600);
  //--------------------------------------------
  Serial.print("Initialisierung ...");
  if (!SD.begin(5)) {                          // Hier muss der CS-Pin angegeben werden, wenn er von 10 abweicht. Wenn die SD-Karte nicht (!SD.begin) gefunden werden kann, ...
    Serial.println(" fehlgeschlagen!");        // ... soll eine Fehlermeldung ausgegeben werden. ....
    return;
  }                                            // Anm.: SD.begin() --> CS = PIN10. Falls anderer PIN gewünscht: z.B. SD.begin(5) für PIN5
  Serial.println(" abgeschlossen");            // ... Ansonsten soll die Meldung "Initialisierung abgeschlossen." ausgegeben werden.
  //--------------------------------------------

  Textdatei = SD.open(dateiName, FILE_WRITE);  // Textdatei wird geöffnet.

  if (Textdatei) {                             // Falls die Textdatei mit Nemen (s.o. -dateiName) gefunden wurde....
    Serial.println("Schreibe in Textdatei...");// ... soll eine Meldung im seriellen Monitor erscheinen...
    Textdatei.println("LSZU");                  // ... und die Textdatei anschließend befüllt werden.(txt wird immer am Ende angehängt)
    Textdatei.print("1, 2, 3, 4, 5, ");
    Textdatei.println("a, b, c, d, e");
    Textdatei.println();

    Textdatei.close();                         // Anschließend wird die Textdatei wieder geschlossen...
    Serial.println("Abgeschlossen.");          // ... und eine erneute Meldung im seriellen Monitor ausgegeben.
    Serial.println();
  }
  else {                                                          // Wenn !keine! Textdatei gefunden werden kann ...
    Serial.println("Textdatei konnte nicht ausgelesen werden");   // ... erscheint eine Fehlermeldung im seriellen Monitor.
  }

  //--------------------------------------------
  // NUN WIRD DIE TEXTDATEI AUSGELESEN und am Seriellen-Monitor ausgegeben - sozusagen als Test, ob alles geklappt hat.
  Textdatei = SD.open(dateiName);                            // Die Textdatei auf der SD-Karte wird wieder geoeffnet...
  if (Textdatei) {
    Serial.print("Auslesen der Datei ");
    Serial.print(dateiName); Serial.println(": ");           // ... und der Name der Datei wird ausgegeben.
    while (Textdatei.available()) {                          // Anschließend wird die Datei so lange Zeile für Zeile
      Serial.write(Textdatei.read());                        // ... ausgelesen (while) bis keine Daten mehr gefunden werden können.
    }
    Textdatei.close();                                       // Im Anschluss wird die Textdatei wieder geschlossen.
  }
  else {                                                     // Falls keine Textdatei mit dem Namen gefunden wurde...
    Serial.println("Oeffnen der Textdatei nicht moeglich");  // ... erscheint eine Fehlermeldung im seriellen Monitor.
  }
  //--------------------------------------------
  // Nun wird die Textdatei in ihrem Inhalt erweitert
  Serial.print("Die Datei"); Serial.print(dateiName); Serial.println("wird erweitert");
  Textdatei = SD.open(dateiName, FILE_APPEND); //Textdatei wird geöffnet
  if (Textdatei) {                             // Falls die Textdatei mit Nemen (s.o. -dateiName) gefunden wurde....
    Serial.println("Schreibe in Textdatei...");// ... soll eine Meldung im seriellen Monitor erscheinen...
    Textdatei.println("Adelsheim");                  // ... und die Textdatei anschließend befüllt werden.(txt wird immer am Ende angehängt)
    Textdatei.print("6, 7, 8, 9, 10, ");
    Textdatei.println("f, g, h, i, j");

    Textdatei.close();                         // Anschließend wird die Textdatei wieder geschlossen...
    Serial.println("Abgeschlossen.");          // ... und eine erneute Meldung im seriellen Monitor ausgegeben.
    Serial.println();
  }
  else {                                                          // Wenn !keine! Textdatei gefunden werden kann ...
    Serial.println("Textdatei konnte nicht ausgelesen werden");   // ... erscheint eine Fehlermeldung im seriellen Monitor.
  }
  //-----------------------------------------------
  //Die Textdatei wird nun erneut ausgelesen
  Textdatei = SD.open(dateiName);                            // Die Textdatei auf der SD-Karte wird wieder geoeffnet...
  if (Textdatei) {
    Serial.print("Auslesen der Datei ");
    Serial.print(dateiName); Serial.println(": ");           // ... und der Name der Datei wird ausgegeben.
    while (Textdatei.available()) {                          // Anschließend wird die Datei so lange Zeile für Zeile
      Serial.write(Textdatei.read());                        // ... ausgelesen (while) bis keine Daten mehr gefunden werden können.
    }
    Textdatei.close();                                       // Im Anschluss wird die Textdatei wieder geschlossen.
  }
  else {                                                     // Falls keine Textdatei mit dem Namen gefunden wurde...
    Serial.println("Oeffnen der Textdatei nicht moeglich");  // ... erscheint eine Fehlermeldung im seriellen Monitor.
  }
  delay(1000);
}

// Die Loop-Schleife bleibt leer. Die Textdatei sollte hier
// als Test ja auch nur einmal beschrieben und ausgelesen werden
void loop() {

}
```