# SD Karte

###  Zielsetzung 
* Verbinden von einem SD Card Modul über die SPI Schnittstelle mit dem ESP32.
* Daten vom ESP32 auf einer SD Karte speichern und von dort lesen. 

###  benötigte Libraries 

```c
#include "FS.h"  // fuer Filehandling
#include "SD.h"  // fuer die SD Card
#include "SPI.h" // spi Schnittstelle
```

###  Verkabelung 
![[esp32-sd_card_steckplatine.svg]]

| MicroSD card module | ESP32    |
| ------------------- | -------- |
| GND                 | GND      |
| VCC                 | 5V(3.3V) |
| MISO                | GPIO 19  |
| MOSI                | GPIO 23  |
| CLK                 | GPIO 18  |
| CS                  | GPIO 5   |

In den Anleitungen steht, dass man den Adapter mit 3,3V verbinden soll, da die SD Karte mit 3,3V läuft. Falls man einen Adapter mit Spannungsregelung hat, kann es sein, dass man VCC an 5V anschließen muss damit die Verbindung klappt. 
Bei unserem Modul kam mit 3,3V der Fehler Card mount failed.

###  Programmierung 

Dies ist das Beispielprogramm aus der Arduino IDE. Dieses findet man unter SD(ESP32) - SD-Test.

```c
// die notewendigen Libraries importieren
#include "FS.h"
#include "SD.h"
#include "SPI.h"

// Methode die die Ordner anzeigt

void listDir(fs::FS &fs, const char * dirname, uint8_t levels){
    Serial.printf("Listing directory: %s
", dirname);

    File root = fs.open(dirname);
    if(!root){
        Serial.println("Failed to open directory");
        return;
    }
    if(!root.isDirectory()){
        Serial.println("Not a directory");
        return;
    }

    File file = root.openNextFile();
    while(file){
        if(file.isDirectory()){
            Serial.print("  DIR : ");
            Serial.println(file.name());
            if(levels){
                listDir(fs, file.name(), levels -1);
            }
        } else {
            Serial.print("  FILE: ");
            Serial.print(file.name());
            Serial.print("  SIZE: ");
            Serial.println(file.size());
        }
        file = root.openNextFile();
    }
}

// Methode die einen Ordner erstellt

void createDir(fs::FS &fs, const char * path){
    Serial.printf("Creating Dir: %s
", path);
    if(fs.mkdir(path)){
        Serial.println("Dir created");
    } else {
        Serial.println("mkdir failed");
    }
}

// Methode die einen Ordner löscht

void removeDir(fs::FS &fs, const char * path){
    Serial.printf("Removing Dir: %s
", path);
    if(fs.rmdir(path)){
        Serial.println("Dir removed");
    } else {
        Serial.println("rmdir failed");
    }
}

// Methode die eine Datei einliest

void readFile(fs::FS &fs, const char * path){
    Serial.printf("Reading file: %s
", path);

    File file = fs.open(path);
    if(!file){
        Serial.println("Failed to open file for reading");
        return;
    }

    Serial.print("Read from file: ");
    while(file.available()){
        Serial.write(file.read());
    }
    file.close();
}

// Methode um eine Datei auf die Karte zu schreiben

void writeFile(fs::FS &fs, const char * path, const char * message){
    Serial.printf("Writing file: %s
", path);

    File file = fs.open(path, FILE_WRITE);
    if(!file){
        Serial.println("Failed to open file for writing");
        return;
    }
    if(file.print(message)){
        Serial.println("File written");
    } else {
        Serial.println("Write failed");
    }
    file.close();
}

// Methode um an eine Datei etwas anzuhängen

void appendFile(fs::FS &fs, const char * path, const char * message){
    Serial.printf("Appending to file: %s
", path);

    File file = fs.open(path, FILE_APPEND);
    if(!file){
        Serial.println("Failed to open file for appending");
        return;
    }
    if(file.print(message)){
        Serial.println("Message appended");
    } else {
        Serial.println("Append failed");
    }
    file.close();
}

// Methode um eine Datei umzubenennen

void renameFile(fs::FS &fs, const char * path1, const char * path2){
    Serial.printf("Renaming file %s to %s
", path1, path2);
    if (fs.rename(path1, path2)) {
        Serial.println("File renamed");
    } else {
        Serial.println("Rename failed");
    }
}

// Methode um eine Datei zu löschen

void deleteFile(fs::FS &fs, const char * path){
    Serial.printf("Deleting file: %s
", path);
    if(fs.remove(path)){
        Serial.println("File deleted");
    } else {
        Serial.println("Delete failed");
    }
}

// 

void testFileIO(fs::FS &fs, const char * path){
    File file = fs.open(path);
    static uint8_t buf[512];
    size_t len = 0;
    uint32_t start = millis();
    uint32_t end = start;
    if(file){
        len = file.size();
        size_t flen = len;
        start = millis();
        while(len){
            size_t toRead = len;
            if(toRead > 512){
                toRead = 512;
            }
            file.read(buf, toRead);
            len -= toRead;
        }
        end = millis() - start;
        Serial.printf("%u bytes read for %u ms
", flen, end);
        file.close();
    } else {
        Serial.println("Failed to open file for reading");
    }

    file = fs.open(path, FILE_WRITE);
    if(!file){
        Serial.println("Failed to open file for writing");
        return;
    }

    size_t i;
    start = millis();
    for(i=0; i<2048; i++){
        file.write(buf, 512);
    }
    end = millis() - start;
    Serial.printf("%u bytes written for %u ms
", 2048 * 512, end);
    file.close();
}

// Im setup stehen bei diesem Sketch alle Aufrufe der Methoden

void setup(){

// Verbindung mit dem Seriellen Monitor herstellen
    Serial.begin(115200);

// Falls keine Verbindung mit dem Adapter herstellbar ist eine Fehlermeldung ausgeben.
    if(!SD.begin()){
        Serial.println("Card Mount Failed");
        return;
    }
// Den SD Kartentyp auslesen
    uint8_t cardType = SD.cardType();

// Falls keine Karte eingelegt ist eine Fehlermeldung ausgeben
    if(cardType == CARD_NONE){
        Serial.println("No SD card attached");
        return;
    }

// Den Kartentyp ausgeben
    Serial.print("SD Card Type: ");
    if(cardType == CARD_MMC){
        Serial.println("MMC");
    } else if(cardType == CARD_SD){
        Serial.println("SDSC");
    } else if(cardType == CARD_SDHC){
        Serial.println("SDHC");
    } else {
        Serial.println("UNKNOWN");
    }

// Die Größe der SD Karte abfragen und ausgeben.
    uint64_t cardSize = SD.cardSize() / (1024 * 1024);
    Serial.printf("SD Card Size: %lluMB
", cardSize);

// Die aktuellen Ordner ausgeben.
    listDir(SD, "/", 0);
// Einen Ordner mit dem Namen mydir erstellen.
    createDir(SD, "/mydir"); 
    listDir(SD, "/", 0);

// Den Ordner mydir entfernen.   
    removeDir(SD, "/mydir");
    listDir(SD, "/", 2);

//  Eine Datei mit dem Namen hello.txt und dem Inhalt Hello auf die SD Karte schreiben.
    writeFile(SD, "/hello.txt", "Hello ");

// An die bestehende Datei hello.txt noch den Text World und ein Zeilenumbruch anhängen.    
    appendFile(SD, "/hello.txt", "World!
");
// Die Datei hello.txt lesen
    readFile(SD, "/hello.txt");

// Die Datei foo.txt löschen.
    deleteFile(SD, "/foo.txt");

// Die Datei hello.txt in foo.txt umbenennen und lesen.
    renameFile(SD, "/hello.txt", "/foo.txt");
    readFile(SD, "/foo.txt");

//  Die Datei test.txt wird mit unserer Test - Methode untersucht.
    testFileIO(SD, "/test.txt");

// printf gibt einen String formatiert aus. 
// In dem String kann mit % ein Platzhalter enthalten sein, der durch verschiedene Befehle formatiert wird.
// llu heißt das der Platzhalter als long long unsigned integer formatiert wird.
// Der String muss mit einem Zeilenumbruch beendet werden 
 .
    Serial.printf("Total space: %lluMB
", SD.totalBytes() / (1024 * 1024));
    Serial.printf("Used space: %lluMB
", SD.usedBytes() / (1024 * 1024));
}

void loop(){

}
```