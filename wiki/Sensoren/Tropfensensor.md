# Tropfensensor

Der Tropfensensor kann sowohl als Füllstandsmesser als auch als Tropfenmesser zur Detektion von Niederschalg eingesetzt werden.
### Zielsetzung

* Messung des Wasserstandes 
* Messung des Niederschlags
### Library 

Keine Library notwendig
###  Verkabelung 
Anschlüsse des MOD-BME 280 (von links nach rechts): Für unser Projekt und die Kommunikation über die I2C-Schnittstelle benötigen wir lediglich die vier Pins am unteren Ende des Sensors:

| Sensor | ESP32 |
|---|---|
| - | GND|
|+|5 V|
|S|z. B. 34|

### Programmierung

```c
/***************************************************************************
  Fortbildung Mikrocontroller LSZU

  Verbindung ESP32 - Tropfensensor
 ***************************************************************************/

int messwert = 0; //Unter der Variablen "messwert" wird später der Messwert des Sensors gespeichert.

void setup()

{

  Serial.begin(9600); //Die Kommunikation mit dem seriellen Port wird gestartet. Das benötigt man, um sich den ausgelesenen Wert im "serial monitor" anzeigen zu lassen.

}

void loop()

{

  messwert = analogRead(34);

  Serial.print("Feuchtigkeits-Messwert:"); //Ausgabe am Serial-Monitor: Das Wort „Feuchtigkeits-Messwert:"

  Serial.println(messwert); //und im Anschluss der eigentliche Messwert.

  delay(500); // Zum Schluss noch eine kleine Pause, damit nicht zu viele Zahlenwerte über den Serial-Monitor rauschen.

}
```