# Lichtschranke
### Zielsetzung

* Eine Lichtschranke in Betrieb nehmen, um Bewegungseriegnisse zu detektiren (z. B.: Insektenflug oder Regenwippe)
* Die Anzahl der Ereignisse zählen und auf dem seriellen Monitor ausgeben
### Library 

  keine Library notwendig
###  Verkabelung 
Der Pin OUT der Lichtschranke kann an jeden beliebigen Pin des Mikrocontrollers angeschlossen werden, der im Pinout mit Input bezeichnet wird. 

| Sensor | ESP32 |
|---|---|
| GND | GND|
|VDD|3,3V|
|OUT|z. B 33|

### Programmierung

```c
/***************************************************************************
Fortbildung Mikrocontroller LSZU

Verbindung ESP32 - Lichtschranke
 ***************************************************************************/

int mess= 33;
int Zaehler=0;
bool vorher=0;
bool aktuell=0;

void setup() {
Serial.begin(9600);
pinMode(mess, INPUT);
}

void loop() {
aktuell=digitalRead(mess);
if(aktuell != vorher) {   //Die Anweisung wird ausgelöst, wenn sich der Wert am Output der Lichtschranke ändert
                          //Zählt man einfach nur während der Output der Lichtschranke auf High ist, so steigt der Werte kontinuierlich an
                          //wenn sich ein Objekt in der Lichtschranke befindet
vorher=aktuell;
Zaehler=Zaehler + 1;      //Der Zähler steigt bei jeder Änderung um 1. Will man INsekten Zählen würde also jedes Insekt, dass die
                          // das die Lichtschranke durchquert den Zähler um 2 steigen lassen. Hier müsste später noch
                          // durch 2 geteilt werden
Serial.print("Anzahl Tropfen: ");
Serial.println(Zaehler);
delay(500);
}

}
```