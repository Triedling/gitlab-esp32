# LM35

###  Zielsetzung 

Messung von Temperaturen, wenn der Sensor Wasserdicht verpackt sein muss und der BME280 nicht funktioniert.

###  Verkabelung 
![[esp32-lm35_steckplatine.svg]]

###  Programmierung 

```c
int pin_LM35=36;
float pinAufloesung = 4096.0;
float vmax=3300.0; // dies ist die Spannung, die max. am Pin gemessen wird
float milliV = 0;
int pinWert = 0;
float tempC = 0;

void setup() {
  Serial.begin(9600);
}

void loop() {
  // den analogen Pin einlesen
  pinWert=analogRead(pin_LM35);
  // den Wert in mV umrechnen
  milliV = pinWert*(vmax/pinAufloesung);
  // die Spannung in Temperatur umrechnen
  tempC = milliV/10;

  // die Werte auf dem Seriellen Monitor ausgeben
  Serial.println(pinWert);
  Serial.println(milliV);

  Serial.print(" Die Temperatur beträgt:");
  Serial.print(tempC);
  Serial.println("°C");
  delay(1000);

}
```