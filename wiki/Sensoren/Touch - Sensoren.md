# Touch

Am ESP32 können 10 Touchsensoren angeschlossen werden. Dazu muss nur ein Kabel von einem der Touchfähigen Pins zu einem metallenen Gegenstand geführt werden.

###  Verkabelung 

Diese Pins können benutzt werden:

| | Touch 0 | Touch 1 | Touch 2 | Touch 3 | Touch 5 | Touch 6 | Touch 7 | Touch 8 | Touch 9 |
| ------- | ------- | ------- | ------- | ------- | ------- | ------- | ------- | ------- | --- |
| Pins    | 4       | 2       | 0       | 15      | 12      | 14      | 27      | 33      | 32  |

![[esp32-touch_steckplatine.png]]

###  Programmierung 
####  einfaches Auslesen 

```c
touchRead(T0);
```
Alternative kann auch der GPIO 4 welcher dem Touch 0 entspricht angesprochen werden.

```c
touchRead(4);
```

####  Mit Interrupt 

Wenn der Mikrocontroller noch andere Aufgaben, außer der Überwachung des Sensors erledigen soll, muss man diesen über einen Interrupt abfragen.

```c
int threshold = 40;           // durch welche Veränderung soll der Interrupt ausgelöst werden
bool touchdetected = false;  // hier wird die Variable, die anzeigt, 
                             // ob der touch Sensor berührt wurde, auf false gesetzt

void gotTouch()
{             // dies ist die Funktion die durch den Interrupt aufgerufen wird
 touchdetected = true;
}

void setup() 
{

  touchAttachInterrupt(T2, gotTouch, threshold);

}

void loop()
{

  // hier kann irgendein Programm laufen, welches die Information in touchdetected benutzt
}
```