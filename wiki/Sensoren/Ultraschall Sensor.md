# Ultraschallsensor

###  Programmierung 
Im folgenden Programm muss nur noch die Entfernungsberechnung ergänzt werden.

```c
int trigPin=6;
int echoPin=4;
int dauer=0;
float entfernung=0;
void setup(){
	pinMode(trigPin,OUTPUT);
	pinMode(echoPin,INPUT);
}
void loop(){
	digitalWrite(trigPin,LOW); // Bevor das Signal gesendet wird sollte sichergestellt sein dass der Pin LOW ist.
	delay(10);
	digitalWrite(trigPin,HIGH); // das ist das Signal
	delay(10);
	digitalWrite(trigPin,LOW); // hier endet das Signal
	dauer=pulseIn(echoPin,HIGH); //wartet auf das High Signal und gibt die Dauer in Mikrosekunden zurück.
	entfernung=sBerechnung(dauer);
}
float sBerechnung(int t){
	// hier die Berechnung ergänzen
        float entf = ...

        return entf;
}
```