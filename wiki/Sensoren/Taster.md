# Taster

## Zielsetzung

* Die Stellung eines tasters mit dem Mikrocontroller erfassen
## Library 
  Keine Library notwendig
  
## Verkabelung mit Pullup oder Pulldown Widerstand
![[ESP32-LauflichtmitTaster_Steckplatine.png]]

##  Verkabelung mit internem Pullup Widerstand
Der Taster wird im "Pullup-Betrieb" mit einem Beinchen am 3,3-V-Pin und mit dem anderen Beinchen an einem Pin der im Pinout als Input ausgewisen ist angeschlossen. Achtung: Die Pins 34, 35.36 und 39 können nicht gewählt werden. Sie besitzen keinen internen Pullup-Wiederstand.
![[taster_steckplatine.png]]

### Programmierung

```c
/***************************************************************************
Fortbildung Mikrocontroller LSZU

Taster
 ***************************************************************************/

int Anzahl = 0;
void setup() {

  Serial.begin(115200);

  pinMode(33, INPUT_PULLUP); //Befehl um den interenen Widerstand des Mikrocontrollers zu nutzen, anstatt eine Pullup-Schaltung auf dem Breadboard aufzubauen
}

void loop() {
  if(digitalRead(33) == 0) {
    Anzahl = Anzahl +1;}    //Jedesmal wenn der Taster gedrückt wird, steigt der Wert der Variable Anzahl um 1
    Serial.print("Der Taster wurde "); //Text in Gänsefüßchen wird als Text wiedergegeben
    Serial.print(Anzahl);              
    Serial.println(" Mal gedrueckt");  //Serial.println() bewirkt einen Zeilenumbruch

 }
```