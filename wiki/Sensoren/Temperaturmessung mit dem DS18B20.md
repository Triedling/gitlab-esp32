# DS18B20

### Zielsetzung

* Messung von Temperatur auch bei Umgebungsfeuchtigkeit
* Anschluss mehrerer Termperatursensoren
### Library 

* DallasTemperature von Miles Burton [[https://github.com/milesburton/Arduino-Temperature-Control-Library]]
* OneWire von PaulStoffregen [[https://www.pjrc.com/teensy/td_libs_OneWire.html]]

###  Verkabelung 
Die Anschlüsse werden hier in den farben des Kabels angegeben, da die Verdrahtung nicht gleich ersichtlich wird. Zwischen Dem hohen Potential (Rot) unf der Datenleitung (Gelb) muss ein 4,7-kΩ-Wiederstand geschalten werden.

| Sensor | ESP32 |
|---|---|
| Schwarz | GND|
|Rot|3,3V|
|Gelb|z. B. 32|

![[ds18b20_steckplatine.png]]

Über die OneWire-Schnittstelle können an einem Anschluss (hier GPIO 32) mehrere Temperatursensoren angeschlossen werden. Die Verkabelung ist in der folgenden Abbildung gezeigt.

![[dreids18b20_steckplatine.png]]

Natürlich können noch andere Bauelemente, wie etwa ein Taster zum Auslösen der Temperaturmessung, ergänzt werden. Eine Beispielschaltung, in der der Taster (INPUT_PULLUP) über den Pin 33 ausgelesen wird.

![[2ds18b20_mit_taster_steckplatine.png]]

### Programmierung

Programmierung des DS18B20 mit nur einem Temperaturfühler

```c
/***************************************************************************
  Fortbildung Mikrocontroller LSZU

  Verbindung ESP32 - DS18B20
  Abgewandelt nach RUI SANTOS
 ***************************************************************************/

#include <OneWire.h>
#include <DallasTemperature.h>

const int oneWireBus = 32;  // GPIO an welche der DS18B20 angeschlossen ist

OneWire oneWire(oneWireBus); // Erzeugen eines ineWIre-Objektes um mit OneWire-Bauteilen zu kommunizieren

DallasTemperature sensors(&oneWire);

void setup() {
  Serial.begin(115200); // Starten des seriellen Monitors
  sensors.begin();        // Starten des Temperatursensors
}

void loop() {
  sensors.requestTemperatures();
  float temperatureC = sensors.getTempCByIndex(0); //Die Temperatur wird in Grad C abgerufen und in der Variable hinterlegt
  float temperatureF = sensors.getTempFByIndex(0); //Die Temperatur wird in Grad F abgerufen und in der Variable hinterlegt
  Serial.print(temperatureC);
  Serial.println("ºC");
  Serial.print(temperatureF);
  Serial.println("ºF");
  delay(5000);
}
```

Über die OneWire-Schnittstelle können mehrere DS18B20 über denselben Pin betrieben werden. Ein Beispiel mit 2 Temperatursensoren und einem Taster zum Starten des Messvorgangs ist hier zu sehen.

```c
/***************************************************************************
  Fortbildung Mikrocontroller LSZU

  Verbindung ESP32 - DS18B20
  Abgewandelt nach RUI SANTOS
 ***************************************************************************/

#include <OneWire.h>
#include <DallasTemperature.h>

#define ONE_WIRE_BUS 32 // GPIO an welche der DS18B20 angeschlossen ist

OneWire oneWire(ONE_WIRE_BUS);

DallasTemperature sensors(&oneWire);

int numberOfDevices; //Anzahl an detektierten Temperatursensoren

DeviceAddress tempDeviceAddress; // In dieser Variablen werden die gefundenen Bauteiladressen gespeichert

void setup() {
  Serial.begin(115200);

  sensors.begin();

  numberOfDevices = sensors.getDeviceCount(); //Die Anzahl der Temperatursensoren wird ermittelt

  Serial.print("Locating devices..."); //Die Bauteile werden auf dem Bus lokalisiert
  Serial.print("Found ");
  Serial.print(numberOfDevices, DEC);
  Serial.println(" devices.");

  // Für jeden Sensor wird die Adresse im seriellen Monitor ausgegeben
  for (int i = 0; i < numberOfDevices; i++) {
    // Search the wire for address
    if (sensors.getAddress(tempDeviceAddress, i)) {
      Serial.print("Bauteil gefunden ");
      Serial.print(i, DEC);
      Serial.print(" mit folgender Adresse: ");
      printAddress(tempDeviceAddress);
      Serial.println();
    } else {
      Serial.print("Ghostdevice gefunden ");
      Serial.print(i, DEC);
      Serial.print(" Adresse konnte nicht ermittelt werden. Kontrolliere die Verkabelung");
    }
  }

  //Schalter um Abfrage zu aktivieren
  pinMode(33, INPUT_PULLUP);

}

void loop() {
  sensors.requestTemperatures(); // Temperaturen werden abgerufen

  if (digitalRead(33) == 0) {
    for (int i = 0; i < numberOfDevices; i++) {
      if (sensors.getAddress(tempDeviceAddress, i)) {
        Serial.print("Temperature for device: ");
        Serial.println(i, DEC);
        float tempC = sensors.getTempC(tempDeviceAddress);
        Serial.print("Temp C: ");
        Serial.print(tempC);
        Serial.print(" Temp F: ");
        Serial.println(DallasTemperature::toFahrenheit(tempC)); // Rechnet Grad Celsius in Grad Fahrenheit um
      }
    }
  }
  delay(500);
}

// function to print a device address
void printAddress(DeviceAddress deviceAddress) {
  for (uint8_t i = 0; i < 8; i++) {
    if (deviceAddress[i] < 16) Serial.print("0");
    Serial.print(deviceAddress[i], HEX);
  }
}
```