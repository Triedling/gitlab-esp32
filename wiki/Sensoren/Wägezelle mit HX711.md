# Wägezelle 

###  Anleitungen 

https://randomnerdtutorials.com/esp32-load-cell-hx711/

###  Zielsetzung 

Mit einer Wägezelle kann man die Masse eines Bienenstocks oder einer Pflanze im zeitlichen Verlauf messen. Dadurch kann beim Bienenstock grafisch dargestellt werden, wann die Bienen ausfliegen, wieviel Honig täglich eingelagert wird, ....
Bei einer Pflanze kann man sehen wann gegossen wird, wie die Pflanze wächst.

###  Library 
Wir benutzen die HX711 Library von Olav Kallhovd. Diese muss unter Bibliotheken verwalten, installiert werden.

https://github.com/olkal/HX711_ADC

#include <HX711_ADC.h>

###  Verkabelung 

![[esp32-hx711_steckplatine.png]]

###  Programmierung 

```c
#include <HX711_ADC.h>

//HX711 constructor (dout pin, sck pin)
HX711_ADC LoadCell(5, 4);

long t;

void setup() {
  Serial.begin(115200);

  Serial.println("Wait...");
  LoadCell.begin();
  long stabilisingtime = 2000; // tare preciscion can be improved by adding a few seconds of stabilising time
  LoadCell.start(stabilisingtime);
  LoadCell.setCalFactor(10000.0); // user set calibration factor (float)
  Serial.println("Startup + tare is complete");
}

void loop() {
  //update() should be called at least as often as HX711 sample rate; >10Hz@10SPS, >80Hz@80SPS
  //longer delay in scetch will reduce effective sample rate (be carefull with delay() in loop)
  LoadCell.update();

  //get smoothed value from data set + current calibration factor
  if (millis() > t + 250) {
    float i = fabs(LoadCell.getData());
    float v = LoadCell.getCalFactor();
    Serial.print("Load_cell output val: ");
    Serial.print(i);
    Serial.print("      Load_cell calFactor: ");
    Serial.println(v);

    // Create a string which is the integer value of the weight times 10,
    //  to remove the decimal point.
    String weight = String(int(i*10));

    // Identify which decimal point to set, and set it.
    int shiftBy = 5-weight.length();
    int decimalPoint = 0x08>>(shiftBy);

    t = millis();
  }

  //receive from serial terminal
  if (Serial.available() > 0) {
    float i;
    char inByte = Serial.read();
    if (inByte == 'l') i = -1.0;
    else if (inByte == 'L') i = -10.0;
    else if (inByte == 'h') i = 1.0;
    else if (inByte == 'H') i = 10.0;
    else if (inByte == 't') LoadCell.tareNoDelay();
    if (i != 't') {
      float v = LoadCell.getCalFactor() + i;
      LoadCell.setCalFactor(v);
    }
  }

  //check if last tare operation is complete
  if (LoadCell.getTareStatus() == true) {
    Serial.println("Tare complete");
  }

}
```