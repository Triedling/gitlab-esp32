# MHZ 19B

###  Zielsetzung 
Mit Hilfe des MHZ 19B Sensors wollen wir eine $CO_2$ Ampel bauen, mit der die Luftqualität in Innenräumen überwacht werden kann.

###  Library 
  *MH-Z19 (von Jonathan Dempsey) [Dokumentation](https://github.com/WifWaf/MH-Z19) 
  *XLR8HardwareSerial (von Alorium Technology) [Dokumentation](https://github.com/AloriumTechnology/XLR8HardwareSerial)

###  Verkabelung 
Die serielle Schnittstelle muss über Kreuz angeschlossen werden, dh. RX vom Sensor zu TX vom Mikrocontroller und umgekehrt. 
Die Kabelfarben sind beim Seonsor etwas speziell gewählt, deshalb in folgender Tabelle eine kleine Hilfestellung.

| Verfügbare Sensoren       | Kabelfarbe                                                  | Pin am Mikrocontroller         |
|---|---|---|
| HD    | orange  | -  |
| Analog Out    | weiß   | -  |
| GND    | blau   | GND       |
| Vin    | grün   | 5V       |
| RxD    | gelb   | TxD (17)      |
| TxD    | schwarz   | RxD (16)       |
| PWM   | rot   | -  |

![[esp32-mhz19-serial_steckplatine.png]]
###  Programmierung 

####  Verbindung zu einer zweiten Seriellen Schnittstelle herstellen 

```c
#include <HardwareSerial.h> // die Library zur Verwaltung von mehreren Seriellen Schnittstellen einbinden

HardwareSerial mySerial(2); // die serielle Schnittstelle 2 liegt an den Pins 16 (RX) und 17 (TX)

void setup()
{

   mySerial.begin(9600); // die Kommunikationsgeschwindigkeit festlegen

}
```

####  Sensor Programmierung =

```c
#include "MHZ19.h"    // die Library für die Kommunikation mit dem Sensor MHZ 19 einbinden

MHZ19 myMHZ19;        // ein MHZ19 Objekt erstellen

void setup()
{
   myMHZ19.begin(mySerial);    // einen Stream vom Sensor zum Mikrocontroller starten

   myMHZ19.autoCalibration();  // die automatische Kalibrierung einschalten

   CO2 = myMHZ19.getCO2();     // einen CO2 Wert einlesen

   Temp = myMHZ19.getTemperature(); // die Temperatur einlesen, !Achtung die Temperaturmessung ist sehr ungenau!
}
```

Von den drei seriellen Schnittstellen des ESP32 kann nur UART2 (RX2: GPIO 16, TX2: GPIO 17) ohne weiteres benutzt werden.
UART0 wird für die USB-Datenübertragung zwischen der IDE auf dem PC und dem Mikrocontroller verwendet. Während der Nutzung dieser zwei Pins kann folglich kein Programm auf den Mikrocontroller geladen und auch keine Daten zur Anzeige am seriellen Monitor auf den Computer übertragen werden.
UART1 ist nur nutzbar,da GPIO 9 un d10 für die Ansteuerung des Flash-Speichers reserviert sind. 

###  ganze Quelltext 

```c
#include <Arduino.h>
#include "MHZ19.h"                                        
#include <HardwareSerial.h>                                

#define BAUDRATE 9600                                     // Mikrocontroller zu MH-Z19 Serial Baudrate (sollte nicht geändert werden)

MHZ19 myMHZ19;                                            // ein MH-Z19-Objekt erstellen
HardwareSerial mySerial(2);                               // Legt die serielle Schnittstelle auf GPIO 16 (RX) und GPIO 17 (TX)

unsigned long getDataTimer = 0;

void setup()
{
    Serial.begin(9600);                                    
    Serial.println("Test");
    mySerial.begin(BAUDRATE);                               
    Serial.println("Starting...");  
    myMHZ19.begin(mySerial);                              // einen Stream vom Sensor zum Mikrocontroller starten 

    myMHZ19.autoCalibration();                            // Schaltet Autokalibrierung an ON (OFF autoCalibration(false))

}

void loop()
{
    if (millis() - getDataTimer >= 2000)
    {
        int CO2; 

        /* note: getCO2() default is command "CO2 Unlimited". This returns the correct CO2 reading even 
        if below background CO2 levels or above range (useful to validate sensor). You can use the 
        usual documented command with getCO2(false) */

        CO2 = myMHZ19.getCO2();                              // Ruft den CO2-Wert ab (in ppm)

        Serial.print("CO2 (ppm): ");                      
        Serial.println(CO2);                                

        int8_t Temp;
        Temp = myMHZ19.getTemperature();                     // Ruft die Temperatur ab (in ° Celsius)
        Serial.print("Temperature (C): ");                  
        Serial.println(Temp);                               

        getDataTimer = millis();
    }
}
```