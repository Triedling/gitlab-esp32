# WS2812 - Neopixel
## Verkabelung
Wenn man nur kleine LED Streifen anschließt reicht die Stromversorgung des ESP32 aus. Bei längeren muss eine externe 5V Spannungsversorgung benutzt werden.
![[ws2812.png]]
## Bibliothek
Wir benutzen die Bibliothek von Adafruit. Diese muss im Librarymanager installiert werden.
```c 
#include <Adafruit_NeoPixel.h> 
```

## Das Neopixel Objekt erstellen
Alle Aktionen werden auf einem Objekt der Klasse Adafruit_NeoPixel ausgeführt.
- ` NUM_PIXELS` stellt die Anzahl der angeschlossenen LEDs ein
- `PIN_NEO_PIXEL` sagt an welchem PIN des ESP32 die Datenleitung angeschlossen ist.
```c
int NUM_PIXELS = 10;
int PIN_NEO_PIXEL = 32;
Adafruit_NeoPixel NeoPixel(NUM_PIXELS, PIN_NEO_PIXEL, NEO_GRB + NEO_KHZ800); 
```
## Das Neopixel Objekt initialisieren
```c 
NeoPixel.begin();
```
## Alle Neopixel LEDs ausschalten
```c 
NeoPixel.clear(); 
```
## Die Farben der einzelnen LEDs setzen
- `pixel` sagt hier einfach nur die wievielte LED gemeint ist. 
- Die Farbe wird als RGB Farbe eingestellt. (rot, grün, blau).
- Die Farben werden nur eingestellt sie sind noch nicht an der LED sichtbar.
```c 
NeoPixel.setPixelColor(pixel, NeoPixel.Color(0, 255, 0)); 
```
## Die Farben anzeigen
Die aktuell eingestellten Farben anzeigen.
```c 
NeoPixel.show(); 
```
## Verkabelung bei externer Spannungsversorgung
R = $470 \Omega$, C = $1000 \mu F$
![](image/ws2812-esp32-externe-Spannung_Steckplatine.png)