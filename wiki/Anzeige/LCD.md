# LCD

### Zielsetzung

* Anzeigen von Messwerten
* Einbinden der Bibliothek
* Grundlegende Befehle zur Bedienung des LCD
### Library 

* LiquidCrystal I2C [Dokumentation](https://github.com/johnrickman/LiquidCrystal_I2C)
* Wire
###  Verkabelung 
Anschlüsse des LCD (von links nach rechts): Für unser Projekt und die Kommunikation über die I2C-Schnittstelle benötigen wir lediglich die vier Pins am aufgelöteten I2C-Modul:

| Sensor | ESP32                  |
| ------ | ---------------------- |
| GND    | GND                    |
| VDD    | 5 V                    |
| SDA    | GPIO 21; alternativ 17 |
| SCL    | GPIO 22; alternativ 16 |

![[lcd_steckplatine.png]]

### Programmierung

```c
/***************************************************************************
Fortbildung Mikrocontroller LSZU

LCD
 ***************************************************************************/

#include <Wire.h>  // Wire Bibliothek hochladen
#include <LiquidCrystal_I2C.h> // Vorher hinzugefügte LiquidCrystal_I2C Bibliothek hochladen
LiquidCrystal_I2C lcd(0x27, 20, 4);  //Hier wird das Display benannt (Adresse/Zeichen pro Zeile/Anzahl Zeilen). In unserem Fall „lcd“. Die Adresse des I²C Displays kann je nach Modul variieren.

void setup()
{
lcd.init(); //Im Setup wird der LCD gestartet
lcd.backlight(); //Hintergrundbeleuchtung einschalten 
delay(500);
lcd.noBacklight(); //Hintergrundbeleuchtung ausschalten
delay(500);
lcd.backlight();
delay(500); 
}

void loop()
{
lcd.setCursor(4,0); //Text soll beim ersten Zeichen in der ersten Reihe beginnen..
lcd.print("Fortbildung"); //In der ersten Zeile soll der Text „Fortsbildung“ angezeigt werden
lcd.setCursor(2,1); //Genauso geht es bei den weiteren drei Zeilen weiter
lcd.print("Mikrocontroller");
lcd.setCursor(6,2);
lcd.print("am LSZU");
lcd.setCursor(5,3);
lcd.print("Adelsheim");
delay(1000);
}
```