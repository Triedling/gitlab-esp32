#  LED dimmen 

### Zielsetzung

* Die Helligkeit einer LED steuern
* mit dem Befehl analogWrite umgehen
### Library 

* analogRead von erropix [Dokumentation](https://github.com/ERROPiX/ESP32_AnalogWrite)

Anders als beim Arduino UNO ist der Befehl analogRead nicht enthalten. Um den Befehl mit dem ESP32 nutezn zu können, muss diese Bibliothek eingebunden werden.

###  Verkabelung 
Die LED wird genauso angeschlossen wie schon in unseren ersten Programmen. Sie erhält einen Vorwiderstand von 220 Ω. Das kurze Beinchen wird mit Ground und das lange Beinchen mit einem Pin verbunden, der im Pinout als Output angegeben ist:

| Sensor | ESP32 |
|---|---|
| kurezes Beinchen | GND|
|langes Beinchen|z. B. 32|

![[blink_steckplatine.png?direct]]

### Programmierung

```c
/***************************************************************************
Fortbildung Mikrocontroller LSZU

LED dimmen
 ***************************************************************************/

#include <analogWrite.h>

// Pin an der die LED angeschlossen ist
int led = 32;

void setup() {
   // pinMode ist nicht notwendig stört aber auch nicht
pinMode (led, OUTPUT);
}

void loop() {
    // Die Helligkeit nimmmt zu (mögliche Werte von 0 bis 255 --> 8 bit)
  for (int helligkeit = 0; helligkeit < 120; helligkeit = helligkeit + 1) { 
    analogWrite(led, helligkeit);
    delay (10);
  }

  // Die Helligkeit nimmt wieder ab
  for (int helligkeit = 120; helligkeit > 0; helligkeit = helligkeit - 1) {
    analogWrite(led, helligkeit);

    delay (10);
  }
}
```