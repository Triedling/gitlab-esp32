#  LED's 

Die einfachste Art ohne PC Informationen anzuzeigen ist durch LEDs. Entweder in Form einer Ampel oder durch mehrere LEDs in einer Reihe, wodurch pro LED der Messwert um einen bestimmten Betrag steigt. 
## Anschluss
Die LED muss mit einem Vorwiderstand ( $\approx 150 \Omega$ )angeschlossen werden. 

![[esp32-3led_steckplatine.svg]]

##  Programmierung

```c 
void setup(){

	pinMode(5, OUTPUT);
} 

void loop(){

	digitalWrite(5,HIGH);
	delay(1000);
	digitalWrite(5,LOW);
	delay(500);
}
```

### Aufgabe:
Wandle das Beispielprogramm so ab, dass alle drei LEDs angesteuert werden.