# Erstes Programm

Gebt folgendes Programm in die IDE ein und schließt am GPIO 5 eine LED mit Vorwiderstand an.
```c
void setup (){
	pinMode(5,OUTPUT);
}

void loop (){
	digitalWrite(5,HIGH);
	delay(1000);
	digitalWrite(5,LOW);
	delay(1000);
}
```
## Erklärungen
In jedem Programm müssen zwei Funktionen enthalten sein.
### void setup ()
Alles was zwischen den geschweiften Klammern { } steht wird beim Start oder Reset des Mikrocontrollers einmal aufgerufen.
```c
void setup(){
			 
} 
```
### void loop ()
Alles was zwischen den geschweiften Klammern { } steht wird während der Mikrocontroller Strom hat immer wieder aufgerufen.
```c
void loop(){
			
} 
```
### pinMode()
pinMode sagt dem MC ob der GPIO als Ausgang oder Eingang benutzt wird.
```c 
pinMode(5,OUTPUT) 
```

### digitalWrite()
digitalWrite verändert den Zustand eines GPIO zu high oder low.
```c 
digitalWrite(5,HIGH) 
```

### delay()
delay pausiert das Programm für die Zeit die in Millisekunden angegeben ist.
```c 
delay(2000) 
```