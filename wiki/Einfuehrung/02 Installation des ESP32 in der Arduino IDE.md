# Installation des ESP32 in der Arduino IDE

Die aktuelle Anleitung wie das ESP32 Board zur Arduino IDE hinzugefügt wird findet man hier:
[[https://docs.espressif.com/projects/arduino-esp32/en/latest/installing.html#installing-using-boards-manager]]

Schritt 1: Öffne die Arduino IDE und öffne unter "Datei" die Voreinstellungen wie in der Abbildung zu sehen.
![[bild1.png]]

Schritt 2: Gib nun unter zusärtzliche Boardverwalter-URLs folgenden Text ein: https://raw.githubusercontent.com/espressif/arduino-esp32/gh-pages/package_esp32_index.json und bestätige mit ok.
![[bild2.png]]

Schritt 3: Unter Werkzeuge>Board>Boardverwalter kannst du nun den Boardverwalter öffnen. 
![[bild3.png]]

Schritt 4: Suche nun nach dem ESP32, wähle den ESP32 by Espressif Systems aus und drücke installieren.
![[bild4.png]]

Schritt 5: Nun lässt sich unter Werkzeuge>Board>Boardverwalter das ESP32 DevModule auswählen. Dies ist unser Board also bitte auswählen. 
![[bild5.png]]

Nun ist die Installation geschafft! Als kleinen Funktionstest führen wir ein mitinstalliertes Beispielprogramm aus. Öffne Datei>Beispiel>WiFi>WifiScan. Schließe den Microcontroller an und lade das Programm hoch.
Nun wirst du eine Funktionalität kennenlernen, die der ESP32 im Gegensatz zum Arduino besitzt.
![[bild6.png]]

##  Installieren einer Bibliothek 
Die Installation einer Bibliothek funktioniert genauso wie bei jedem andern von der Arduino IDE unterstützen Mikrocontrollern.
Schritt 1: Öffne die Bibliotheksverwaltung unter Werkzeuge>Bibliotheken verwalten.
![[bild7.png]]

Schritt 2: Suche nach der gewünschten Bibliotek in der Suchleiste. Hier Beispielsweise die adafruit BME280 (Ihr werdet sie bestimmt während der Forbildung brauchen). Markiert sie und drückt auf installieren. Anschließend könnt ihr das Fenster wieder schließen.
![[bild8.png]]