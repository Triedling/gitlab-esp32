# Bienenstockwaage

###  Externe Anleitungen 
http://www.randolphesser.de/imkerei/projekte/BeeIoT_v20.html

###  Zielsetzung 

###  Materialien 
* ESP32
* HX711 (von Sparkfun)
* DS18B20 Temperatursensor verkapselt
* BME280 Temperatur, Luftfeuchtigeit, Luftdruck Sensor unverkapselt
* Streifenrasterplatine
* Schraubklemmen mit 2,54mm Abstand

###  Bibliotheken 
hx711 von Bogde

###  Programmierung 
#####  Einbindung der Bibliothek 

```c
#include "HX711.h"
```

#####  Erstellen eines Objekts  

```c
HX711 loadcell;
```

#####  Konstanten definieren 

```c
// Anschlüsse
const int LOADCELL_DOUT_PIN = 2;
const int LOADCELL_SCK_PIN = 3;

// Kalibrierung
const long LOADCELL_OFFSET = 50682624;
const long LOADCELL_DIVIDER = 5895655;
```

#####  Initialisierung des Objekts  

```c
loadcell.begin(LOADCELL_DOUT_PIN, LOADCELL_SCK_PIN);
loadcell.set_scale(LOADCELL_DIVIDER);
loadcell.set_offset(LOADCELL_OFFSET);
```

#####  Abfrage einer Messung   

```c
weight = loadCell.get_units(10);
```



###  Kalibrierung 

- Rufe_scale() ohne Parameter auf.
- Rufe tare() ohne Parameter auf.
- Lege ein bekanntes Gewicht auf die Waage und rufe get_units(10) auf.
- Teile das Ergebnis durch dein bekanntes Gewicht. Das Ergebnis ist der Kalibrierungsfaktor der mit set_scale() übergeben wird.
- Der Kalibrierungsfaktor muss noch getestet und feinjustiert werden.

###  Fertiges Programm 

```c
#include <WiFi.h>
#include <HTTPClient.h> 
#include <HX711.h>
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>

#define SEALEVELPRESSURE_HPA (1013.25)
#define uS_TO_S_FACTOR 1000000ULL  /* Umrechnung von myS zu Sekunden */
#define TIME_TO_SLEEP  20        /* Dauer des deepsleep in Sekunden */

Adafruit_BME280 bme;

//HX711 constructor (dout pin, sck pin)
HX711 loadCell;

// Definition und Initialisierung der Variablen 
float temperature, humidity, pressure, altitude,weight;
float korrektur_faktor = 21.165;
int sda = 4;
int scl = 5;
int sck_hx711 = 19;
int dt_hx711 = 18;
const char* ssid = "SSID";
const char* password =  "Passwort";

//
String sensId = "1";
String hostName = "http://172.16.1.222/neue_messung";
String query ="?";

void connect(const char* ssid,const char* passw){

  WiFi.begin(ssid, passw);

  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to WiFi..");
  }

  Serial.println("Connected to the WiFi network");
}

void getRequest(String query){
  if ((WiFi.status() == WL_CONNECTED)) { 
      HTTPClient http;
      http.begin(query);
      int httpCode = http.GET();                                        

      if (httpCode > 0) {
          String payload = http.getString();
          Serial.println(httpCode);
          Serial.println(payload);
        }

      else {
        Serial.println("Error on HTTP request");
        Serial.println(httpCode);
      }

      http.end();
    }
}

void setup() {

  Serial.begin(115200);

  loadCell.begin(dt_hx711, sck_hx711);
  loadCell.set_scale(korrektur_faktor); 

  Serial.println("Tara...entferne alles von der Waage.");
  delay(500);
  loadCell.tare();
  Serial.println("Tara beendet!");
  delay(1000);

  //legt die Pins fuer die I2C Verbindung fest und startet diese
  Wire.begin(sda,scl);
  bme.begin(0x76); 

  // ruft die Funktion auf, die zum Wlan verbindet
  connect(ssid,password);

  // Abfrage der Sensoren und Aufbau der query
  query="";
  weight = loadCell.get_units(10);

  Serial.println(weight);
  temperature = bme.readTemperature();
  Serial.println(temperature);
  humidity = bme.readHumidity();
  Serial.println(humidity);
  pressure = bme.readPressure() / 100.0F;
  Serial.println(pressure);
  altitude = bme.readAltitude(SEALEVELPRESSURE_HPA); 
  Serial.println(altitude); 

  // zusammensetzen des get Request
  query = hostName +"?"+"sensId=" +sensId + "&temp="+String(temperature) +"&hum="+String(humidity)+"&pres="+String(pressure)+"&wgt="+String(weight);

  // ruft die Funktion auf, die den get-Request absetzt
  getRequest(query);

  // hier beginnt die deepsleep Vorbereitung
  esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * uS_TO_S_FACTOR);
  Serial.println("Setup ESP32 to sleep for every " + String(TIME_TO_SLEEP) + " Seconds");  
  // hier startet der deepsleep
  esp_deep_sleep_start();
  // danach wird nichts mehr ausgefuehrt, auch kein loop!

}

void loop() {

}
```