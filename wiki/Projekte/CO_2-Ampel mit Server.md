# CO2 Ampel

##  Zielsetzung 
Mit Hilfe des MHZ 19B Sensors wollen wir eine $CO_2$ Ampel bauen, mit der die Luftqualität in Innenräumen überwacht werden kann.
Die Messwerte sollen sich einerseits optisch an farbigen LEDs ablesen, andererseits die Werte von einer Homepage ablesen lassen. 
###  Library 
  *MH-Z19 (von Jonathan Dempsey) [Dokumentation](https://github.com/WifWaf/MH-Z19) 
  XLR8HardwareSerial (von Alorium Technology) [Dokumentation](https://github.com/AloriumTechnology/XLR8HardwareSerial)
  WiFi [Dokumentation](https://www.arduino.cc/reference/en/libraries/wifi/)
  WebServer
  WiFiClient

###  Verkabelung 
Die serielle Schnittstelle muss über Kreuz angeschlossen werden, dh. RX vom Sensor zu TX vom Mikrocontroller und umgekehrt. 
Die Kabelfarben sind beim Seonsor etwas speziell gewählt, deshalb in folgender Tabelle eine kleine Hilfestellung.

| Verfügbare Sensoren       | Kabelfarbe                                                  | Pin am Mikrocontroller         |
|---|---|---|
| HD    | orange  | -  |
| Analog Out    | weiß   | -  |
| GND    | blau   | GND       |
| Vin    | grün   | 5V       |
| RxD    | gelb   | TxD (17)      |
| TxD    | schwarz   | RxD (16)       |
| PWM   | rot   | -  |

![[esp32-mhz19-serial_steckplatine.png]]

###  Programmierung 

```c
#include "MHZ19.h"
#include <HardwareSerial.h>
#include <WiFi.h>
#include <WebServer.h>
#include <WiFiClient.h>

int CO2;    // muss schon hier eingeführt werden und nicht erst in Void loop, da man den Wert für CO2 sonst nicht zur Steuerung der led nutzen kann
int led_gruen = 19;
int led_gelb = 18;
int led_rot = 5;
int grenzgruen = 600;
int grenzgelb = 1000;

const char* ssid = "pinkyundbrain";  // Hier SSID eingeben
const char* password = "Weltherrschaft";  //Hier Passwort eingeben

WebServer server(80);

#define BAUDRATE 9600                                      // Device to MH-Z19 Serial baudrate (should not be changed)

MHZ19 myMHZ19;// Constructor for library

HardwareSerial mySerial(2);                                // Hier kann vorgegeben werden, welche Pins angesteuert werden. Myserial(0) 4 (TX) 5 (RX)--> sind diese Pins angeschlossen kann keine Kummunikation mit dem Comupter stattfinden
// myserial (2) die serielle Schnittstelle 2 liegt an den Pins 16 (RX) und 17 (Tx)

unsigned long getDataTimer = 0;

void setup()
{
  Serial.begin(115200);
  Serial.println("Connecting to ");
  Serial.println(ssid);

  //connect to your local wi-fi network
  WiFi.begin(ssid, password);

  //check wi-fi is connected to wi-fi network
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected..!");
  Serial.print("Got IP: ");
  Serial.println(WiFi.localIP());

  server.on("/", handle_OnConnect);
  server.onNotFound(handle_NotFound);

  server.begin();
  Serial.println("HTTP server started");

  Serial.println("Test");
  mySerial.begin(BAUDRATE);           // Baudrate für die serielle Kommunkation zwischen Sensor und Mikrocontroller
  Serial.println("Starting...");
  myMHZ19.begin(mySerial);                                // Kommunikation zwischen Sensor und Mikrocontroller wird gestartet

  myMHZ19.autoCalibration();         // Turn auto calibration ON (OFF autoCalibration(false))
  pinMode(led_gruen, OUTPUT);
  pinMode(led_gelb, OUTPUT);
  pinMode(led_rot, OUTPUT);

}

void loop()
{
  server.handleClient();
  if (millis() - getDataTimer >= 2000)
  {

    /* note: getCO2() default is command "CO2 Unlimited". This returns the correct CO2 reading even
      if below background CO2 levels or above range (useful to validate sensor). You can use the
      usual documented command with getCO2(false) */

    CO2 = myMHZ19.getCO2();                             // CO2-Abfrage (als ppm)

    Serial.print("CO2 (ppm): ");
    Serial.println(CO2);

    int8_t Temp;
    Temp = myMHZ19.getTemperature();                    // Temperaturabfrage (als Celsius)
    Serial.print("Temperature (C): ");
    Serial.println(Temp);

    getDataTimer = millis();

  }                                                    //Lässt die analoge LED-Ampel je nach vorgegebenen Grenzwerten leuchten
  if (CO2 < grenzgruen)
  {
    digitalWrite(led_gruen, HIGH);
    digitalWrite(led_gelb, LOW);
    digitalWrite(led_rot, LOW);
  }
  else if (CO2 >= grenzgruen && CO2 < grenzgelb)
  {
    digitalWrite(led_gruen, LOW);
    digitalWrite(led_gelb, HIGH);
    digitalWrite(led_rot, LOW);
  }
  else if (CO2 >= grenzgelb)
  {
    digitalWrite(led_gruen, LOW);
    digitalWrite(led_gelb, LOW);
    digitalWrite(led_rot, HIGH);
  }
}
// Erstellen der HTML-Seite auf dem Server
void handle_OnConnect() {
  server.send(200, "text/html", SendHTML(CO2));
}
void handle_NotFound() {
  server.send(404, "text/plain", "Not found");
}

String SendHTML(int CO2) {
  String ptr = "<!DOCTYPE html>";
  ptr += "<html>";
  ptr += "<head>";
  ptr += "<title>CO2-Ampel Büro 1110</title>";
  ptr += "<meta name='viewport' content='width=device-width, initial-scale=1.0'>";
  ptr += "<link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,600' rel='stylesheet'>";
  ptr += "<style>";
  ptr += "html { font-family: 'Open Sans', sans-serif; display: block; margin: 0px auto; text-align: center;color: #444444;}";
  ptr += "body{margin: 0px;} ";
  ptr += "h1 {margin: 50px auto 30px;} ";
  ptr += ".side-by-side{display: table-cell;vertical-align: middle;position: relative;}";
  ptr += ".text{font-weight: 600;font-size: 19px;width: 200px;}";
  ptr += ".reading{font-weight: 300;font-size: 50px;padding-right: 25px;}";
  ptr += ".CO2 .reading{color: #F29C1F;}";
  ptr += ".superscript{font-size: 17px;font-weight: 600;position: absolute;top: 10px;}";
  ptr += ".data{padding: 10px;}";
  ptr += ".container{display: table;margin: 0 auto;}";
  ptr += ".icon{width:65px}";
  ptr += "</style>";
  ptr += "</head>";
  ptr += "<body>";
  ptr += "<h1>Landesschulzentrum fuer Umweltbildung</h1>";
  ptr += "<h3>CO2-Ampel Raum 1110</h3>";
  ptr += "<div class='container'>";
  ptr += "<div class='data CO2'>";

  ptr += "</div>";
  ptr += "<div class='side-by-side text'>CO2</div>";
  ptr += "<div class='side-by-side reading'>";
  ptr += (int)CO2;
  ptr += "<span class='superscript'>ppm</span></div>";

  ptr += "</div>";
  ptr += "</body>";
  ptr += "</html>";
  return ptr;

}
```