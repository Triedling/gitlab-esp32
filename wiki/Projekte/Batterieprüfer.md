# Batterieprüfer

### Zielsetzung

* Messung de Spannung einer Batterie und Rückschluss auf deren Ladezustand
* Mit dem Begfehl analogRead umgehen
* Einfache Variablenumformungen durchführen
### Library 

keine Library notwendig
###  Verkabelung 
Die Batterie wird händisch über zwei Kabel mit Ground und einem Pin der im Pinout als Input ausgewiesen ist verbdunden.
Die LEDs werden jeweils über einen 220-Ω-Widerstand mit ground und einem Pin der im Pinout als Output gekennzeichnet ist verbdunden.

| Sensor | ESP32 |
|---|---|
| --Pol | GND|
|+-Pol|z. B 36|
| kurze Beinchen | GND|
|lange Beinchen|z. B 5 und 19 |

![[batterietester_steckplatine.png]]

### Programmierung

```c
/***************************************************************************
Fortbildung Mikrocontroller LSZU

Batterieprüfer
 ***************************************************************************/

int sensor = 36;    //Der Pin an dem das hohe Potential der Batterie gemessen wird
float messWert;
float u;
int ledgelb = 19;   //Die Pins an die die LEDs angeschlossen werden
int ledgruen = 5;

void setup() {

  Serial.begin(9600);
  pinMode(ledgelb, OUTPUT);
  pinMode(ledgruen, OUTPUT);
}

void loop() {

  digitalWrite(ledgruen, LOW);    //Die LEDs werden ausgeschalten, um die Messung einer neuen Batterie zu ermöglichen
  digitalWrite(ledgelb, LOW);
  messWert = analogRead(sensor);
  u = messWert / 4095 * 3.3;      //Aus dem Wert an Pin 36 wird eine Spannung errechnet
  Serial.print("Spannung in V: ");
  Serial.println(u);
  if (u >= 1.4) {                //Die Werte müssen je nach Nennspannung der geprüften Batterie angepasst werden
    digitalWrite(ledgruen, HIGH);
  }
  if (u < 1.4 && u > 1.0) {
    digitalWrite(ledgelb, HIGH);
  }
  delay(500);
}
```