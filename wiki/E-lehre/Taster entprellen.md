# Taster entprellen - Hardware

https://www.youtube.com/watch?v=PBDXOVaepug

### Aufgabe: 
Baue folgende Schaltung auf und schreibe ein Programm welches auf dem Seriellen Monitor ausgibt wie oft der Taster seit Programmstart gedrückt wurde. R   $\approx 30 k \Omega$ .
Was beobachtest du?
![[image/Taster-nicht-entprellt_Steckplatine 1.png]]

## Erklärung
 Man nennt das Phänomen Prellen eines Tasters. Das ist ein mechanisches Problem. Beim schließen der Kontakte prellen diese  kurzzeitig wieder von einander weg. Da unser Mikrocontroller sehr schnell ist misst er das. 
 
 Man kann dies mit Hilfe eines Oszilloskops sichtbar machen.

![[image/Pasted image 20240205020334.png]]

## Elektrotechnische Lösung - Entprellen durch ein RC Glied

Das RC Glied puffert das hin und her springen des Signals ab. Als RC Glied haben wir hier R= $1k \Omega$ und  C = $1 \mu F$ verwendet.

![[image/Taster-entprellt_Steckplatine 1.png]]
### Aufgabe:
Teste diese Schaltung mit deinem Programm.