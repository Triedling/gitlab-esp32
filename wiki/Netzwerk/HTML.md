# html
Eine sehr gute Anleitung für HTML findet man unter:

[[https://wiki.selfhtml.org/wiki/Wie_fange_ich_an%3F]]

##  Grundgerüst einer HTML Datei 

```html
<!doctype html>
<html lang="de">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Beschreibung der Seite</title>
  </head>
  <body>
    <p>Inhalt der Seite</p>
  </body>
</html>
```

Html-Elemente können entweder nur aus einer Klammer wie im Grundgerüst <!doctype html> bestehen oder aus einem öffnenden und einem schließenden Teil, wie <head  > bis </head   >, falls sich der Befehl auf einen Bereich des Dokuments bezieht.

* html - umschließt die ganze Seite
* head – Informationen über die Seite, die von Programmen wie z. B. Suchmaschinen oder Browsern ausgewertet werden
* title - wird im Browser oder an anderen Stellen angezeigt
* body – die eigentlichen, vom Browser darzustellenden Informationen
* p - fett gedruckt
* h1, h2, h3, ... - Überschriften mit Größenangabe

## Sonderzeichen

https://wiki.selfhtml.org/wiki/Zeichenreferenz

Achtung Hochkomma weglassen!

## Automatischer Refresh

https://www.w3schools.com/tags/att_meta_http_equiv.asp
###  Daten darstellen 

###  Buttons