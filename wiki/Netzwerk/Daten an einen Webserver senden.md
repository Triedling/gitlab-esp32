# Wetterdaten an einen Webserver durch get-requests senden mit deepsleep 

## Was ist ein get-request?
Dies ist eine Methode um Daten von einem Client an einen Webserver zu schicken. Die Daten werden mit der URL mitgeschickt.
```python 
http://www.meinserver.de?name1=value1&name2=value2 
```
Nach der eigentlichen Adresse steht ein `?` und danach werden die Daten jeweils mit `&`getrennt aufgelistet.
Dies funktioniert auch wenn man die Daten in die Adresszeile des Browsers eingibt.


http://10.0.0.9/neue_messung?sensId=42&temp=90&hum=70&pres=1011&wgt=100


###  Libraries 
####  `<WiFi.h> `
Stellt die Funktionen für eine Wifi Verbindung bereit.

####  `<HTTPClient.h>` 
Stellt Funktionen eines HTTP Clients bereit. (ähnlich zu einem Browser)

####  `<Wire.h> `
Stellt I2C Verbindungen bereit.

####  `<Adafruit_Sensor.h> `
Ermöglicht die einfache Benutzung von Sensoren die auf Adafruit Sensoren basieren.

####  `<Adafruit_BME280.h> `
Ermöglicht den einfachen Zugriff auf die Daten des BME280.


## Beispielprogramm ohne Deepsleep
Es sendet Wetterdaten die mit dem bme280 gemessen wurden an einen Webserver, der die Daten weiterverarbeitet. 

```c 
#include <WiFi.h>
#include <HTTPClient.h> 

#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>

#define SEALEVELPRESSURE_HPA (1013.25)


Adafruit_BME280 bme;

// Definition und Initialisierung der Variablen 
float temperature, humidity, pressure, altitude;
int sda = 4;
int scl = 5;
const char* ssid = "SSID_eintragen";
const char* password =  "Passwort";

// die sensId ist dazu da dass der Server verschiedene Sensoren unterscheiden kann.
String sensId = "1";
String hostName = "http://10.0.0.9/neue_Messung";
String query ="?";

void verbinde(const char* ssid,const char* passw){

  WiFi.begin(ssid, passw);

  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Verbinde zu WiFi..");
  }

  Serial.println("Verbunden zum WiFi Netzwerk");

}

void getRequest(String query){
  if ((WiFi.status() == WL_CONNECTED)) { 
      HTTPClient http;
      http.begin(query);
// hier werden die Daten gesendet!
      int httpCode = http.GET();                                        
// es ist schon alles gesendet, wir warten nur noch auf die Antwort des Servers
      if (httpCode > 0) {
          String payload = http.getString();
          Serial.println(httpCode);
          Serial.println(payload);
        }

      else {
        Serial.println("Error on HTTP request");
        Serial.println(httpCode);
      }

      http.end();
    }
}

void setup() {

  Serial.begin(115200);

  //legt die Pins fuer die I2C Verbindung fest und startet diese
  Wire.begin(sda,scl);

  // legt die I2C Adresse des BME fest und startet die Verbindung zum BME280
  bme.begin(0x76); 

  // ruft die Funktion auf, die zum Wlan verbindet
  verbinde(ssid,password);



  
}

void loop() {
// Stand Februar 2024 erwartet die Funktion auf dem Server folgende Daten: 
// neue_messung(sensId:int, temp:float, hum: int, pres: int, wgt:int)
  // Abfrage der Sensoren und Aufbau der query
  query="";
  temperature = bme.readTemperature();
  Serial.println(temperature);
  humidity = bme.readHumidity();
  Serial.println(humidity);
  pressure = bme.readPressure() / 100.0F;
  Serial.println(pressure);
  altitude = bme.readAltitude(SEALEVELPRESSURE_HPA); 
  Serial.println(altitude); 
  query = hostName +"?"+"sensId=" +sensId + "&temp="+String(temperature) +"&hum="+String(humidity)+"&pres="+String(pressure)+"&alt="+String(altitude);

  // ruft die Funktion auf, die den get-Request absetzt
  getRequest(query);
  delay(10000);
} 
```


##  Beispiel Programm mit Deepsleep
Es sendet Wetterdaten die mit dem bme280 gemessen wurden an einen Webserver, der die Daten weiterverarbeitet. Der Mikrocontroller schläft die meiste Zeit und erwacht regelmäßig zum Messen und Senden.

```c
#include <WiFi.h>
#include <HTTPClient.h> 

#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>

#define SEALEVELPRESSURE_HPA (1013.25)

#define uS_TO_S_FACTOR 1000000ULL  /* Umrechnung von myS zu Sekunden */
#define TIME_TO_SLEEP  20        /* Dauer des deepsleep in Sekunden */

Adafruit_BME280 bme;

// Definition und Initialisierung der Variablen 
float temperature, humidity, pressure, altitude;
int sda = 4;
int scl = 5;
const char* ssid = "SSID_eintragen";
const char* password =  "Passwort";

// die sensId ist dazu da dass der Server verschiedene Sensoren unterscheiden kann.
String sensId = "1";
String hostName = "http://10.0.0.9/neue_Messung";
String query ="?";

void verbinde(const char* ssid,const char* passw){

  WiFi.begin(ssid, passw);

  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Verbinde zu WiFi..");
  }

  Serial.println("Verbunden zum WiFi Netzwerk");

}

void getRequest(String query){
  if ((WiFi.status() == WL_CONNECTED)) { 
      HTTPClient http;
      http.begin(query);
// hier werden die Daten gesendet!
      int httpCode = http.GET();                                        
// es ist schon alles gesendet, wir warten nur noch auf die Antwort des Servers
      if (httpCode > 0) {
          String payload = http.getString();
          Serial.println(httpCode);
          Serial.println(payload);
        }

      else {
        Serial.println("Error on HTTP request");
        Serial.println(httpCode);
      }

      http.end();
    }
}

void setup() {

  Serial.begin(115200);

  //legt die Pins fuer die I2C Verbindung fest und startet diese
  Wire.begin(sda,scl);

  // legt die I2C Adresse des BME fest und startet die Verbindung zum BME280
  bme.begin(0x76); 

  // ruft die Funktion auf, die zum Wlan verbindet
  verbinde(ssid,password);

// Stand Februar 2024 erwartet die Funktion auf dem Server folgende Daten: 
// neue_messung(sensId:int, temp:float, hum: int, pres: int, wgt:int)
  // Abfrage der Sensoren und Aufbau der query
  query="";
  temperature = bme.readTemperature();
  Serial.println(temperature);
  humidity = bme.readHumidity();
  Serial.println(humidity);
  pressure = bme.readPressure() / 100.0F;
  Serial.println(pressure);
  altitude = bme.readAltitude(SEALEVELPRESSURE_HPA); 
  Serial.println(altitude); 
  query = hostName +"?"+"sensId=" +sensId + "&temp="+String(temperature) +"&hum="+String(humidity)+"&pres="+String(pressure)+"&alt="+String(altitude);

  // ruft die Funktion auf, die den get-Request absetzt
  getRequest(query);

  // hier beginnt die deepsleep Vorbereitung
  esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * uS_TO_S_FACTOR);
  Serial.println("Setup ESP32 to sleep for every " + String(TIME_TO_SLEEP) + " Seconds");  
  // hier startet der deepsleep
  esp_deep_sleep_start();
  // danach wird nichts mehr ausgefuehrt, auch kein loop!
}

void loop() {

}
```
