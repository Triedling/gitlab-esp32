# Thingspeak

###  Zielsetzung 

###  Verkabelung 

###  Programmierung 

```c
#include <WiFi.h>
#include "ThingSpeak.h"
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>

Adafruit_BME280 bme; 

const char* ssid = "EBG_LEHRER";
const char* password = "!Die4zigRaeuber";

WiFiClient  client;
unsigned long Channel_ID = 1668989;
const char * API_key = "JG05TCNRUG8S6XLS";

unsigned long delayTime;
unsigned long lastTime;
int sda = 4;
int scl = 5;

float temperature;
float humidity;
float pressure;

void setup() {
    Serial.begin(115200);
    while(!Serial);    
    Serial.println("BME280");

    unsigned status;

    // default Einstellungen
    Wire.begin(sda,scl);        // hier wird die Verkabelung zum bme280 übergeben
    status = bme.begin(0x77);  // hier muss die Adresse stehen, die man mit I2C-Test herausgefunden hat

    if (!status) {
        Serial.println("Es ist kein passender BME280 sensor angeschlossen, stimmt die Verkabelung, die Adresse, Sensor ID!");
        Serial.print("SensorID was: 0x"); Serial.println(bme.sensorID(),16);
        while (1) delay(10);
    }

    Serial.println("-- default Test --");
    delayTime = 1000;

    Serial.println();

    verbindeWiFi(ssid,password);   

    ThingSpeak.begin(client);  // Initialisiere ThingSpeak
}

void loop() { 

    if ((millis() - lastTime) > delayTime) {

    temperature = bme.readTemperature();
    humidity = bme.readHumidity();
    pressure = bme.readPressure();

    printWerte(temperature, humidity, pressure);

    sendeTS(temperature, humidity,pressure);

    lastTime = millis();
  }

}

void verbindeWiFi(const char* ssid, const char* password) {
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  Serial.print("Verbinde mit WiFi ..");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print('.');
    delay(1000);
  }
  Serial.println(WiFi.localIP());
}

void sendeTS(float temperature, float pressure, float humidity){
    ThingSpeak.setField(1, temperature);
    ThingSpeak.setField(2, pressure);
    ThingSpeak.setField(3, humidity);

    int Data = ThingSpeak.writeFields(Channel_ID, API_key);

    if(Data == 200){
      Serial.println("Channel updated successfully!");
    }
    else{
      Serial.println("Problem updating channel. HTTP error code " + String(Data));
    } 
}
void printWerte(float temperature, float pressure, float humidity) {

    Serial.print("SensorID ist: 0x"); Serial.println(bme.sensorID(),16);
    Serial.print("Temperatur = ");
    Serial.print(temperature);
    Serial.println(" *C");

    Serial.print("Luftdruck = ");

    Serial.print(pressure/ 100.0F);
    Serial.println(" hPa");

    Serial.print("Luftfeuchtigkeit = ");
    Serial.print(humidity);
    Serial.println(" %");

    Serial.println();
}
```