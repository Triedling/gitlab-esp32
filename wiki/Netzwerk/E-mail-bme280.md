# BME280 - Mail komplettes Programm
```c
#include <WiFi.h>

#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>

#define SEALEVELPRESSURE_HPA (1013.25)

Adafruit_BME280 bme;

#include "ESP_Mail_Client.h"

// Variablen für bme280 
float temperature, humidity, pressure, altitude;
int sda = 4;
int scl = 5;

// Einstellungen fuer Wifi
const char* WIFI_SSID = "SSID";
const char* WIFI_PASSWORD = "Passwort";

// Einstellungen fuer E-Mail

#define SMTP_server "smtp.1und1.de"
#define SMTP_Port 587

#define sender_email "esp32_test@riedling.eu"
#define sender_password "lszu2023"

#define Recipient_email "weis@lszu.de"
#define Recipient_email2 "riedling@lszu.de"

SMTPSession smtp;

// Inhalt der Messungen

String nachricht = "Temperatur,Luftfeuchtigkeit, Luftdruck, Hoehe ";

String befehl;

void wifi_verbinde(){
// attempt to connect to Wifi network:
  Serial.print("Connecting to Wifi SSID ");
  Serial.print(WIFI_SSID);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  while (WiFi.status() != WL_CONNECTED)
  {
    Serial.print(".");
    delay(500);
  }
  Serial.print("
WiFi connected. IP address: ");
  Serial.println(WiFi.localIP());

}

void schicke_mail(String text){

  smtp.debug(1);

// Ein Objekt der Klasse ESP_Session deklarieren
  ESP_Mail_Session session;

  session.server.host_name = SMTP_server ;
  session.server.port = SMTP_Port;
  session.login.email = sender_email;
  session.login.password = sender_password;
  session.login.user_domain = "";

// Ein Objekt der Klasse SMTP_Message deklarieren 
  SMTP_Message message;

  message.sender.name = "ESP32";
  message.sender.email = sender_email;
  message.subject = "ESP32 Testing Email";
  //message.addRecipient("Microcontrollerslab",Recipient_email);
  message.addRecipient("Microcontrollerslab Haha",Recipient_email2);

//Die Nachricht als Textnachricht senden

  message.text.content = text.c_str();
  message.text.charSet = "us-ascii";
  message.text.transfer_encoding = Content_Transfer_Encoding::enc_7bit; 

  if (!smtp.connect(&session))
    return;

  if (!MailClient.sendMail(&smtp, &message))
    Serial.println("Error sending Email, " + smtp.errorReason());

}
void setup() {

  // Die Serielle Verbindung starten
  Serial.begin(115200);

  //legt die Pins fuer die I2C Verbindung fest und startet diese
  Wire.begin(sda,scl);

  // legt die I2C Adresse des BME fest und startet die Verbindung zum BME280
  bme.begin(0x77); 

  wifi_verbinde();

}

void loop() { 

// Wenn die serielle Verbindung verfuegbar ist und der Befehl sende eingetippt wird, dann sende die Mail.

  if(Serial.available()) {
      befehl=Serial.readStringUntil('\n');
      if(befehl="sende"){
        schicke_mail(nachricht);
      }
  }

  temperature = bme.readTemperature();
  Serial.println(temperature);
  humidity = bme.readHumidity();
  Serial.println(humidity);
  pressure = bme.readPressure() / 100.0F;
  Serial.println(pressure);
  altitude = bme.readAltitude(SEALEVELPRESSURE_HPA); 
  Serial.println(altitude); 
  nachricht += String(temperature)","+String(humidity)+","+String(pressure)+","+String(altitude)+" \n ";
  delay(5000);

}
```