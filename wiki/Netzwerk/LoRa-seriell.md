# LoRa Boards mit Serieller Schnittstelle - ebyte


## Anschluss ESP32 zu Lora Modul


https://www.instructables.com/ESP32-With-E32-433T-LoRa-Module-Tutorial-LoRa-Ardu/

## Pins des ebyte lora moduls
| Pin No. | Pin item | Pin direction         | Pin application                                                                                                                                                                                                                   |
| ------- | -------- | --------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 1       | M0       | Input（weak pull-up） | Work with M1 & decide the four operating modes. Floating is not allowed; it can be ground.                                                                                                                                        |
| 2       | M1       | Input（weak pull-up） | Work with M0 & decide the four operating modes. Floating is not allowed; it can be ground.                                                                                                                                        |
| 3       | RXD      | Input                 | TTL UART inputs connect to external (MCU, PC) TXD output pin. It can be configured as open-drain or pull-up input.                                                                                                                |
| 4       | TXD      | Output                | TTL UART outputs connect to external RXD (MCU, PC) input pin. Can be configured as open-drain or push-pull output                                                                                                                 |
| 5       | AUX      | Output                | To indicate the module’s working status & wake up the external MCU. During the procedure of self-check initialization, the pin outputs a low level. It can be configured as open-drain or push-pull output (floating is allowed). |
| 6       | VCC      |                       | Power supply 3V~5.5V DC                                                                                                                                                                                                           |
| 7       | GND      |                       | Ground                                                                                                                                                                                                                            |

## modes die über M0 und M1 eingetstellt werden

| Mode            | M1  | M0  | Explanation                                                                  |
| --------------- | --- | --- | ---------------------------------------------------------------------------- |
| Normal          | 0   | 0   | UART and wireless channels are open, and transparent transmission is on      |
| WOR Transmitter | 0   | 1   | WOR Transmitter                                                              |
| WOR Receiver    | 1   | 0   | WOR Receiver (Supports wake up over air)                                     |
| Deep sleep mode | 1   | 1   | The module goes to sleep (automatically wake up when configuring parameters) |

## Zum Testen
Man kann auf einem Android Handy "Serial USB Terminal installieren" und ein ebyte Lora board über ein ftdi

## Receiver

```c 
HardwareSerial SerialLora(1);

void setup() {
	Serial.begin(9600);
	SerialLora.begin(9600,SERIAL_8N1,16,17);
}

  

void loop() {
// Wenn Daten vom Lora Modul ankommen,
// werden diese über den Seriellen Monitor angezeigt.
	if (SerialLora.available()){
		Serial.write(SerialLora.read());
	}
} 
```

## Transceiver / Sender

```c 
HardwareSerial SerialLora(1);
int count = 0;

void setup() {
	SerialLora.begin(9600,SERIAL_8N1,16,17);
}

void loop() {
	count++;
	SerialLora.print("hallo");
	SerialLora.println(count);
	delay(2000);
} 
```