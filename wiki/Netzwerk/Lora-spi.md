# LoRa Boards mit SPI Schnittstelle

## Lora API
https://github.com/sandeepmistry/arduino-LoRa/blob/master/API.md

## Programmierung Sender
```c
/*********
  Siehe auch Bsp. Arduino LoRa library
*********/

#include <SPI.h>
#include <LoRa.h>

// Die SPI Pins für die Verbindung zum Lora Modul definieren
#define ss 5
#define rst 14
#define dio0 2

# Nummer des gesendeten Pakets
int counter = 0;

void setup() {

  //Seriellen Monitor
  Serial.begin(115200);
  while (!Serial);
  Serial.println("LoRa Sender");

  //Die Pins in das Lora Modul eintragen
  LoRa.setPins(ss, rst, dio0);
  
  //in LoRa.beginn die richtige Frequenz eintragen
  //433E6 Asien oder Europe
  //866E6 Europe
  //915E6 Amerika
  while (!LoRa.begin(866E6)) {
    Serial.println(".");
    delay(500);
  }
   // Das sync word (0xF3) an den Empfaenger anpassen
  // Der Empfaenger hört nur auf Nachrichten mit dem richtigen sync word 
  // syncWord kann im Bereich 0x0-0xFF liegen
  LoRa.setSyncWord(0xF3);
  Serial.println("LoRa  OK!");
}

void loop() {
// Status Ausgabe im Seriellen Monitor
  Serial.print("Das Paket wird gesendet: ");
  Serial.println(counter);

// Senden eines LoRa Pakets zum Empfaenger
  LoRa.beginPacket();
  LoRa.print("Hallo ");
  LoRa.print(counter);
  LoRa.endPacket();

  counter++;

  delay(10000);
}
```

## Empfänger Programmieren

Der Empfänger unterscheidet sich nur im loop. 

```c

void loop() {
  // schauen ob ein Paket ankommt
  int packetSize = LoRa.parsePacket();
  // wenn etwas ankommt
  if (packetSize) {
    
    Serial.print("Received packet '");

    // Paket lesen und an Seriellen Monitor schicken
    while (LoRa.available()) {
      String LoRaData = LoRa.readString();
      Serial.print(LoRaData); 
    }

    // Ausgabe des RSSI des Pakets. Gibt die Signalstaerke an
    Serial.print("' with RSSI ");
    Serial.println(LoRa.packetRssi());
  }
}
```