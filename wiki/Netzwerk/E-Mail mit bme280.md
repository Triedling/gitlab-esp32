# BME280 E-Mail Erklärung
Die Sensordaten des BME280 werden auf einen Befehl aus dem Seriellen Monitor hin im csv Format per Mail gesendet.

###  Library 

https://github.com/mobizt/ESP-Mail-Client

###  Library einbinden 

```c
#include "ESP_Mail_Client.h"
```

###  Mail Einstellungen 

```c
#define SMTP_server "Sever_zum_Senden"
#define SMTP_Port 587

#define sender_email "Benutzername_Mail_Konto"
#define sender_password "Passwort_Mail_Konto"

#define Recipient_email "empfaenger@Mail-adresse.de"
```

###  Funktion zum Senden der Mail 

```c
void schicke_mail(String text){

  smtp.debug(1);

// Ein Objekt der Klasse ESP_Session deklarieren
  ESP_Mail_Session session;

// Einstellungen in das Session Objekt schreiben

  session.server.host_name = SMTP_server ;
  session.server.port = SMTP_Port;
  session.login.email = sender_email;
  session.login.password = sender_password;
  session.login.user_domain = "";

// Ein Objekt der Klasse SMTP_Message deklarieren 

  SMTP_Message message;

// Einstellungen in das message Objekt schreiben

  message.sender.name = "ESP32";
  message.sender.email = sender_email;
  message.subject = "ESP32 Testing Email";
  //message.addRecipient("Microcontrollerslab",Recipient_email);
  message.addRecipient("Microcontrollerslab Haha",Recipient_email2);

//Art der Nachricht einstellen

  message.text.content = text.c_str();
  message.text.charSet = "us-ascii";
  message.text.transfer_encoding = Content_Transfer_Encoding::enc_7bit; 

// Falls keine Verbindung zum Mailserver dann brich die Funktion ab

  if (!smtp.connect(&session))
    return;

// Falls die Mail nicht gesendet werden kann, dann gib die Fehlermeldung aus.

  if (!MailClient.sendMail(&smtp, &message))
    Serial.println("Error sending Email, " + smtp.errorReason());

}
```

###  Die Nachricht senden 

```c
// Wenn die serielle Verbindung verfuegbar ist und der Befehl sende eingetippt wird, dann sende die Mail.

  if(Serial.available()) {
      befehl=Serial.readStringUntil('
');
      if(befehl="sende"){
        schicke_mail(nachricht);
      }
  }
```

###  Die Nachricht mit Messwerten befüllen 

```c
nachricht += String(temperature) +","+String(humidity)+","+String(pressure)+","+String(altitude)+"
 ";
```