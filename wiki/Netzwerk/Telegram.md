# Telegram
###  Vorbereitung eines Telegram Bots 

Ein Telegram Bot ist ein Service der auf Telegram läuft und jeder Nutzer einrichten kann.

1. In Telegramm nach dem botfather suchen und diesen starten

2. Einen neuen bot anlegen. Entweder über das Menü oder den Befehl /newbot

3. Einen Namen und einen username für den bot anlegen indem man einfach die Fragen beantwortet.

4. Man erhält jetzt eine Nachricht mit dem bottoken. Diesen muss man sich notieren.

5. In Telegram nach dem idbot suchen und starten. 

6. Mit dem Befehl /getid bekommt man seine eigene Nutzer ID genannt. Diese muss man sich auch notieren.

###  Programmierung 

```c
#include <WiFi.h>
#include <WiFiClientSecure.h>
#include <UniversalTelegramBot.h>
#include <ArduinoJson.h>

// Hier die eigenen Netzwerkdaten eintragen
const char* WIFI_SSID = "ssid";
const char* WIFI_PASSWORD = "Passwort";

// Den Telegram BOT initialisieren
#define BOT_TOKEN "Telegramm_Token"  // deinen Bot Token (vom Botfather)

// wird nur benötigt wenn man überprüfen will ob der Befehlt vom richtigen Telegramm Account kam.
#define CHAT_ID "Deine_Chat_ID"

const unsigned long BOT_MTBS = 1000; // Pause zwischen der erneuten Nachfrage beim Bot

WiFiClientSecure secured_client;
UniversalTelegramBot bot(BOT_TOKEN, secured_client);
unsigned long bot_lasttime;          // die Zeit der letzten Nachfrage beim Bot

const int ledPin = 26;
int ledStatus = 0;

void handleNewMessages(int numNewMessages)
{
  Serial.print("Neue Nachrichten werden abgefragt");
  Serial.println(numNewMessages);

  for (int i = 0; i < numNewMessages; i++)
  {
    String chat_id = String(bot.messages[i].chat_id);
    if (chat_id != CHAT_ID )
    {
      bot.sendMessage(chat_id, "Du kommst hier net rein!", "");
    }
    else
    {
      String text = bot.messages[i].text;

      String from_name = bot.messages[i].from_name;
      if (from_name == "")
        from_name = "Guest";

      if (text == "/ledon")
      {
        digitalWrite(ledPin, HIGH); // die LED anschalten 
        ledStatus = 1;
        bot.sendMessage(chat_id, "Led ist an", "");
      }

      if (text == "/ledoff")
      {
        ledStatus = 0;
        digitalWrite(ledPin, LOW); // Die LED ausschalten
        bot.sendMessage(chat_id, "Led ist an", "");
      }

      if (text == "/status")
      {
        if (ledStatus)
        {
          bot.sendMessage(chat_id, "Led ist an", "");
        }
        else
        {
          bot.sendMessage(chat_id, "Led ist aus", "");
        }
      }

      if (text == "/start")
      {
        String welcome = "Herzlich Willkommen bei dem LSZU Telegram Bot, " + from_name + ".
";
        welcome += "Das ist ein LED Beispiel

";
        welcome += "/ledon : LED anschalten
";
        welcome += "/ledoff : LED ausschalten
";
        welcome += "/status : Den Status der LED abfragen
";
        bot.sendMessage(chat_id, welcome, "Markdown");
      }
    }
  }
}

void setup()
{
  Serial.begin(115200);
  Serial.println();

  pinMode(ledPin, OUTPUT); 
  delay(10);
  digitalWrite(ledPin, LOW); 

  // Verbindungsversuch mit dem Wifi Netzwerk
  Serial.print("Verbinde mit Wifi SSID ");
  Serial.print(WIFI_SSID);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  secured_client.setCACert(TELEGRAM_CERTIFICATE_ROOT); // Das root Zerfikat hinzufügen api.telegram.org
  while (WiFi.status() != WL_CONNECTED)
  {
    Serial.print(".");
    delay(500);
  }
  Serial.print("
WiFi ist verbunden mit IP Addresse: ");
  Serial.println(WiFi.localIP());
  Serial.print("Uhrzeit der Abfrage: ");
  configTime(0, 0, "pool.ntp.org");
  time_t now = time(nullptr);
  while (now < 24 * 3600)
  {
    Serial.print(".");
    delay(100);
    now = time(nullptr);
  }
  Serial.println(now);

}

void loop()
{
  if (millis() - bot_lasttime > BOT_MTBS)
  {
    int numNewMessages = bot.getUpdates(bot.last_message_received + 1);
    Serial.println(millis());
    Serial.println(numNewMessages);
    while (numNewMessages)
    {
      Serial.println("Ich habe eine Antwort.");
      handleNewMessages(numNewMessages);
      numNewMessages = bot.getUpdates(bot.last_message_received + 1);
    }

    bot_lasttime = millis();
  }
}
```