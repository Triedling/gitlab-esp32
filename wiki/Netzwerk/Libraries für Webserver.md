# libraries für Webserver

| Library             | Besonderheit                                | Start Befehl | weitere Befehle |
  | ------------------- | ------------------------------------------- | ------------ | --------------- |
  | Wifi.h              | zum herstellen von Wlan Verbindungen        |              |                 |
  | WebServer.h         | Standard für Webserver- nur eine Verbindung |              |                 |
  | wifiwebserver.h     | gut dokumentiert                            |              |                 |
  | ESPAsyncWebWerver.h | mehrere Verbindungen gleichzeitig           |              |                 |

#include <WiFi.h>
#include <WiFiClient.h>
#include <WiFiAP.h>
#include <WebServer.h>
#include <AsyncTCP.h>???
#include <ESPAsyncWebServer.h>

###  wifi.h 

Wird immer benötigt um eine Wlan Verbindung herzustellen. Kann aber auch einen Webserver zur Verfügung stellen.
####  Befehle 

###  WebServer.h 
Wartet auf Anfragen eines clients.
####  Befehle / Grundgerüst 

```c
// import der Library
#include <WebServer.h>

// ein Server Objekt erstellen welches auf Port 80 hört
WebServer server(80); 

void setup(){

// Einstiegspunkte definieren welche dann Funktionen aufrufen
server.on("/", funktion1);
server.on("/led1on", funktion2);

server.onNotFound(handle_NotFound);

// den Server starten
server.begin();

}

void loop(){

// die Anfragen des Clients behandeln
server.handleClient();

}

// die einzelnen Funktionen definieren die aufgerufen werden
void funktion1 (){
   server.send(200, "text/html", SendHTML(true)); 
}

void funktion2 (){
   server.send(200, "text/html", SendHTML(false)); 
}
void handle_NotFound (){
   server.send(404, "text/plain", "Not found");

}

// die Funktion die einen String der HTML Seite herstellt
String SendHTML(boolean b){
   String ptr ="...";
   ptr+="";
   if(b){
      ptr += "";
   }
   else{
      ptr += "";
   }
   return ptr;

}
```

###  wifiwebserver.h 

#####  Usage 

Class Constructor
  WiFiWebServer server(80);
Creates the WiFiWebServer class object.

Parameters:

host port number: int port (default is the standard HTTP port 80)

#####  Basic Operations 

Starting the server

  void begin();
Handling incoming client requests

  void handleClient();
Disabling the server

  void close();
  void stop();
Both methods function the same

Client request handlers

  void on();
  void addHandler();
  void onNotFound();
  void onFileUpload();	

###  asyncwebserver.h