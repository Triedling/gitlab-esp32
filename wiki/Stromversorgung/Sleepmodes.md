# Sleepmodes

##  Nutzen der Schlafmodi 
Soll der Mikrocontroller über einen längeren Zeitraum mit einer externen Energiequelle (z. B.: Powerbank) betrieben werden, die dabei nicht - durch beispielsweise Solarzellen - geladen wird, muss auf die Energie nutzung geachtet werden. 
Der ESP32 bietet die Möglichkeit Teile außer Betrieb zu setzen und somit die Energienutzung zu verringern. 
Der Modus sollte je nach Bedarf gewählt werden. Es existieren folgende Modi mit abnehmendem Energiebedarf:
* Active
* Modem Sleep
* Light Sleep
* Deep Sleep
* Hibernation

Wir werden lediglich auf Light Sleep und Deep Sleep näher eingehen.

##  Quellen zum tieferen Einlesen 

[Sketche zu den verschiedenen Schlafmodi](https://m1cr0lab-esp32.github.io/sleep-modes/sleep-modes/)

[Beschreibung der Schlafmodi](https://lastminuteengineers.com/esp32-sleep-modes-power-consumption/)