# Lightsleep

###  Ziel  
Um Sensoren in der Natur zu platzieren, müssen sie meist längere Zeit mit einer begrenzten Energiemenge(Batterie) auskommen.
Mit LightSleep kann der Mikrocontroller in einen Schlafmodus versetzt werden, in dem er nur noch sehr wenig Strom braucht, der Hauptspeicher jedoch aktiv bleibt. Im Vergleich zum DeepSleep-Modus nutzt er dadurch deutlich mehr Energie, die Inhalte von Variablen bleiben dadurch allerdings erhalten.

###  Quelle 
[[https://randomnerdtutorials.com/esp32-deep-sleep-arduino-ide-wake-up-sources/]]

###  Minimal - Programm 

```c
long zeit_in_mikrosekunden = 1000000; // = 1 Sekunde
void setup(){

   meine_funktion();

   // definiert welche Art des Aufwachens passieren soll
   esp_sleep_enable_timer_wakeup(zeit_in_mikrosekunden);

   // hier wird der MC schlafen gelegt
   esp_light_sleep_start();
}  
void meine_funktion(){
  //hier passieren die Dinge die ich machen möchte wenn ich wach bin

}

void loop(){
// loop wird nie aufgerufen!!!
}
```

###  Umstrukturierung eines Sketchs zur Nutzung von Deep Sleep 

Wir werden hier lediglich das "Aufwachen" des MC nach einer vorgegeben Zeit präsentieren. Alternativ könnte der MC auch durch ein externes Signal "aufgeweckt" werden, Ein funktionierender Sketch kann ohne weiteres um den Deep-Sleep-Modus ergänzt werden. Dabei bieten sich folgendes Vorgehen an:
- Nach jedem Aufwachen führt der Mikrocontroller den gesamten Sketch, inklusive des Setups, aus. Soll ein Teil des Programms also immer wieder ausgeführt werden, genügt es ihn ins Setup zu schreiben. Das Programm sollte so umstrukturiert werden, dass alle Bestandteile (außer der Defintion weiterer Funktionen) im Setup steht. Loop verbleibt leer.
- Beim zeitbasierten Aufwachen wird die Zeit zwischen den Messwerten durch die Schlafenzeit vorgegeben. Es sollte daher überlegt werden, weitere delays, die ursprünglich die Zeitspanne zwischen 2 Messungen definiert haben, zu löschen.

###  Beispiel 

Als Beispiel soll hier der BME280 mit Speicherung der Daten auf eine SD-Karte gezeigt werden.

```c
/***************************************************************************
  Fortbildung Mikrocontroller LSZU

  Verbindung ESP32 - BME280 mit SD-Karte und light sleep
 ***************************************************************************/

#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>
#include <SPI.h>    //Bibliothek für die Kommnikation mit der SD-Karte
#include <SD.h>     //Bibliothek für die SD-Karte

#define SEALEVELPRESSURE_HPA (1013.25)

File Textdatei;                   // An dieser Stelle wird die Variable "Textdatei" als File (dts. Datei) deklariert.
String dateiName = "/lszu.txt";   //Dateinamen angeben. Vor den Dateinamen muss ein / gesetzt werden
int messwerte[] = {0, 1, 2, 3};   //Es wird ein Array für die Aufnahme der Messwerte erzeugt
String dataString = "Temperatur in °C:; Luftdruck in hPa:; Höhe über Normalnull:; Luftfeuchtigkeit in %
";           //Benennung der Tabellenspalten mit /n wird ein Zeilenumbruch eingefügt

Adafruit_BME280 bme;

unsigned long delayTime;
int sda = 21;             // es können auch andere GPIO als SDA bzw. SCL verwendet werden
int scl = 22;             // die hier gewählten sind die bei unserem Mikrocontroller voreingestellten GPIOs

long zeit_in_mikrosekunden = 1000000; // = 1 Sekunde. Nach dieser Zeit wacht der Mikrocontroller wieder auf

void setup() {

  Serial.begin(115200);
  while (!Serial);

  initSD();

  Serial.println("BME280");

  unsigned status;

  // default Einstellungen
  Wire.begin(sda, scl);       // hier wird die Verkabelung zum bme280 übergeben
  status = bme.begin(0x76);   // hier muss die Adresse stehen, die man mit I2C-Test herausgefunden hat entweder 0x76 oder 0x77

  if (!status) {
    Serial.println("Es ist kein passender BME280 sensor angeschlossen, stimmt die Verkabelung, die Adresse, Sensor ID!");
    Serial.print("SensorID was: 0x"); Serial.println(bme.sensorID(), 16);
    while (1) delay(10);
  }

  Serial.println("-- default Test --");
  delayTime = 1000;

  Serial.println();

  printValues();
  messwerte[0] = bme.readTemperature();
  messwerte[1] = bme.readPressure() / 100.0F;
  messwerte[2] = bme.readAltitude(SEALEVELPRESSURE_HPA);
  messwerte[3] = bme.readHumidity();
  speichern();

  delay(delayTime);

  esp_sleep_enable_timer_wakeup(zeit_in_mikrosekunden);   // definiert welche Art des Aufwachens passieren soll

  esp_light_sleep_start();                                // hier wird der MC schlafen gelegt
}

void loop() {

}

void printValues() {

  //Serial.print("SensorID ist: 0x"); Serial.println(bme.sensorID(),16);  // Die SensorID kann ausgegeben werden, um zu unterscheiden,
  Serial.print("Temperatur = ");                                          // welcher Sensor angeschlossen ist (BMP180, BMP085, BMP280, BME280, BME 680)
  Serial.print(bme.readTemperature());
  Serial.println(" *C");

  Serial.print("Luftdruck = ");
  Serial.print(bme.readPressure() / 100.0F); //Umrechnung von Pa in hPa. Das F stellt sicher, dass durch eine Gleitkommazahl geteilt wird
  Serial.println(" hPa");

  Serial.print("Höhe über Meeresspiegel = ");
  Serial.print(bme.readAltitude(SEALEVELPRESSURE_HPA));
  Serial.println(" m");

  Serial.print("Luftfeuchtigkeit = ");
  Serial.print(bme.readHumidity());
  Serial.println(" %");

  Serial.println();
}

void speichern() {

  //Fülle den DatenString mit Messwerten aus den Array in dem die Messwerte
  //gespeichert sind. Hierzu "addiert" man das nächste gewünschte Zeichen
  //an das Ende des Datenstrings an. Achtung: Es muss ein ZEICHEN sein und
  //keine Zahl, deshalb wandelt man Integer erst noch in einen String um.
  //Zum Trennen der Tabellenspalten für das Tabellen-Programm zur Auswertung
  //fügt man nach jedem Messwert ein Semikolon an.

  for (int i = 0; i <= 3; i++) {                    //Die Schleife wird durchlaufen, bis alle aufgenommen Messwerte in den String eingefügt wurden
    dataString = dataString + String(messwerte[i]); //Dazu wird die neue Zeile an den bisherigen String angefügt
    dataString = dataString + ";";                  //Um die Daten als CSV-Datei zu importieren werden sie durch ein Semikolon abgegrenzt

  }

  dataString = dataString + "
";                    // Abschließend erfolgt ein Zeilenumbruch

  Textdatei = SD.open(dateiName, FILE_WRITE);
  if (Textdatei) {
    Textdatei.println(dataString);
    Textdatei.close();
    Serial.println("Daten gepeichert!");
    delay(10);
    Serial.print(dataString);
  }

  else {
    Serial.println("Fehler beim Öffnen der Datei");
  }

}

void initSD() {
  Serial.print("Initialisierung ...");
  if (!SD.begin(5)) {                          // Wenn die SD-Karte nicht (!SD.begin) gefunden werden kann, ...
    Serial.println(" fehlgeschlagen!");        // ... soll eine Fehlermeldung ausgegeben werden. ....
    while (1);
  }
  Serial.println(" abgeschlossen");            // ... Ansonsten soll die Meldung "Initialisierung abgeschlossen." ausgegeben werden.

}
```