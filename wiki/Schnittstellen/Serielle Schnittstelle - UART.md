UART = Universal Asynchronous Receiver/Transmitter 

Alternative Erklärung:

https://edistechlab.com/wie-funktioniert-uart/?v=3a52f3c22ed6

https://de.wikipedia.org/wiki/Universal_Asynchronous_Receiver_Transmitter

###  Bausteine die diese Schnittstelle benutzen 

[[Untitled | MHZ19]]

###  Funktionsweise 

Es werden Daten seriell, dh. hintereinander als Bits gesendet. Es wird je ein empfangender Pin/RX (read) und ein sendender Pin/TX (transmit) benötigt. Der Sender und der Empfänger müssen auf die gleiche Geschwindigkeit eingestellt sein damit die Kommunikation funktioniert.
###  Daten Rahmen 
Sender (TX) und Empfänger (RX) müssen sich auf einen so genannten Rahmen geeinigt haben. 

Dh.:
* Wie lang ist das Datenpaket
* Wie lang und wie viele Stopp Bits werden gesendet
* Eine ungefähre Datenrate, die genaue Synchronisation wird vom Empfänger jeweils für einen Rahmen berechnet.(Bytesynchron)

![[rs-232_timing.svg | 300]]

* Wenn keine Daten übertragen werden wird ein andauerndes Highsignal gesendet.
* Das Senden eines 8Bit Datenpakets beginnt mit dem Senden eines Start Bits. 
* Danach folgenden die 8Bit Daten, die allerdings auch ein Prüfungs Bit ein so genanntes Paritiy Bit enthalten. 
* Nach den 8 Bit Daten folgen 1-2 Stopp Bits.

Das Paritiy Bit gibt an ob die Anzahl der 1er in den 7 Bit Daten gerade oder ungerade war. Ist das Parity Bit 1 waren eine gerade Zahl 1er enthalten, bei einer 0 war es eine ungerade Anzahl.

###  Anschluss 

* Die beiden Datenleitungen (RX, TX) müssen immer über Kreuz verbunden werden. Dh. RX vom einen Gerät zu TX vom anderen Gerät. 
* Wenn man den ESP32 über ein USB Kabel mit dem PC verbunden hat, darf man nicht die RX TX Pins an GPIO1 und GPIO3 benutzen, da diese parallel geschaltet sind.
* Der ESP32 hat 3 Serielle Schnittstellen

###  Programmierung 
####  Library einbinden 

Ist für neuere Versionen nicht notwendig!

```c
#include <HardwareSerial.h>
```

####  Vorgefertigte Instanzen 

```c
Serial       // an GPIO1 und GPIO3
Serial1      // an GPIO9 und GPIO10
Serial2      // an GPIO16 und GPIO17
```

####  eigene Instanz erstellen 
Ist nur notwendig, wenn nicht die vorgefertigten Instanzen benutzt werden sollen. 

```c
HardwareSerial meineSerial(2);      // eine Instanz der Seriellen Schnittstelle 2
```

####  Kommunikation starten und Instanzen verändern 

```c
Serial.begin(115200);     // die UART0 Schnittstelle mit 115200Baud starten
Serial1.begin(115200);    // die UART1 Schnittstelle mit 115200Baud an den Standardpins starten
Serial2.begin(115200,SERIAL_8N1, 19, 20); // die UART2 Schnittstelle mit 115200 Baud, 
                                      //dem Protokoll 8N1, an den Pins 19 und 20 starten
mySerial.begin(9600);
```

[[https://github.com/G6EJD/ESP32-Using-Hardware-Serial-Ports/blob/master/ESP32_Using_Serial2.ino]]

###  Demo mit einem ESP32 

[![esp32-rx-tx_steckplatine.svg]]
Es kann über den Seriellen Monitor Daten geschickt und im gleichen Monitor wieder empfangen werden.

```c
#include <HardwareSerial.h>
String message;

void setup() {
  // die Geschwindigkeit für die Serielle Kommunikation festlegen
  // ! die gleiche Geschwindigkeit muss im Seriellen Monitor eingestellt sein !
  Serial.begin(115200);
  Serial2.begin(115200);
}

void loop() {
  if (Serial2.available()) {
    message = Serial2.readString();
    delay(1000);
    Serial.print(message);
  }
  if (Serial.available()) {
    message = Serial.readString();
    delay(1000);
    Serial2.write(message);
  }
}
```
###  Mehrere Serielle Geräte anschließen