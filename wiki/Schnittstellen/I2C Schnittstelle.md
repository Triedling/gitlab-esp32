###  Bausteine die diese Schnittstelle benutzen 
[[BME 280 | BME280]]

[[Wägezelle mit HX711 | HX711]]

###  Funktionsweise 
I2C ist ein Bussystem. Dh. an den beiden Leitungen können mehrere Geräte angeschlossen sein, welche dann über eine Adresse angesprochen werden können. 

###  Verkabelung Beispiel 

![[esp32-i2c_steckplatine.svg]]

###  I2C Scanner 
Der folgende I2C Scanner ist auch in der IDE in den Beispielen unter wire zu finden. Er ist hilfreich, wenn bei einem Sensor die Adresse nicht bekannt ist.

```c
// I2C address Scanner code  
#include <Wire.h>  
 void setup()
 {
   Wire.begin(21,22);
   Serial.begin(9600);
   while (!Serial);             // Leonardo: wait for serial monitor
   Serial.println("I2C Scanner");
 }
 void loop()
 {
   byte error, address;
   int nDevices;
   Serial.println("Scanning…");
   nDevices = 0;
   for (address = 1; address < 127; address++ )
   {
     // The i2c_scanner uses the return value of
     // the Write.endTransmisstion to see if
     // a device did acknowledge to the address.
     Wire.beginTransmission(address);
     error = Wire.endTransmission();
     if (error == 0) {   
        Serial.print("I2C device found at address 0x");   
        if (address < 16)     
           Serial.print("0");   
        Serial.print(address, HEX);   
        Serial.println("  !");   
        nDevices++; 
      } 
      else if (error == 4) {   
        Serial.print("Unknown error at address 0x");   
        if (address < 16)     
           Serial.print("0");   
        Serial.println(address, HEX); 
      }
 }
   if (nDevices == 0)
     Serial.println("No I2C devices found");
   else
     Serial.println("done");
 delay(5000);           // wait 5 seconds for next scan
 }
```