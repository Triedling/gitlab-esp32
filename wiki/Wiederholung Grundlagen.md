## Zielsetzung

* Vertraut machen mit dem ESP32
* Wiederholung der wichtigsten Programmstrukturen
* Stecken von einfachen Schaltungen auf dem Breadboard
## Library 

* LiquidCrystal I2C [Dokumentation](https://github.com/johnrickman/LiquidCrystal_I2C)
* Wire
###  Aufgaben 
- LED-Lauflicht
    * Schließe eine LED mit Vorwiderstand an und lasse sie blinken (Wähle mit dem Pinout einen passenden Pin).
    * Baue ein Lauflicht aus mindestens 5 LEDs. 
    * Nutze nun für das Lauflicht eine for-Schleife und erstelle dir für die genutzen Pins ein Array.
    * Lasse das Lauflicht auch wieder zurücklaufen
- Bedienung über einen Taster
    * Baue zusätzlich einen Taster ein und lasse das Lauflicht erst starten, wenn der Taster erstmalig gedrückt wurde.
    * Lasse das Lauflicht nur rückwärts laufen, wenn der Taster gedrückt wird, zähle wie oft das Lauflicht rückwärts durchlaufen wurde und gib den Zahlenwert über den seriellen Monitor aus.
- Schließe eine LCD an und gib dort aus, wie oft das Lauflicht rückwärts gelaufen ist. (Achtung Bibliothek notwendig. Daten für LCD bitte aus Sketch weiter unten übernehmen)
- Gestsalte deinen Sketch mit Funktionen übersichtlicher:
    * Erstelle eine Funktion, die die Ansteuerung der LEDs übernimmt (keine Datenübergabe an Funktion und keine Datenrückgabe von Funktion)
    * Erstelle eine Funktion die die angibt, wie oft das Lauflicht rückwärts gelaufen ist. (Datenübergabe an dei Funktion)
    * Erstelle eine Funktion die Ausgibt, wie oft das Lauflicht rückwärts gelaufen ist.(Datenrückgabe von Funktion)

### Programmierung
####  Aufgabe 1 
#####  LED blinken lassen 

```c
void setup() {

pinMode(32,OUTPUT);
}

void loop() {
digitalWrite(32,1);
delay(500);
digitalWrite(32,0);
delay(500);

}
```

#####  Lauflicht mit 7 LEDs 

```c
int led [7] = {32,33,25,26,27,14,12};
int paus = 200;
void setup() {
for(int i=0; i<7; i++){
  pinMode(led[i],OUTPUT); 
}

}

void loop() {
for(int i=0; i<7; i++){
digitalWrite(led[i],1);
delay(paus);
digitalWrite(led[i],0);
delay(paus/10); 
}

}
```

#####  Lauflicht rückwärts 

```c
int led [7] = {32, 33, 25, 26, 27, 14, 12};
int paus = 200;
void setup() {
  for (int i = 0; i < 7; i++) {
    pinMode(led[i], OUTPUT);
  }

}

void loop() {
  for (int i = 0; i < 7; i++) {
    digitalWrite(led[i], 1);
    delay(paus);
    digitalWrite(led[i], 0);
    delay(paus / 10);
  }
  for (int i = 5; i >= 0; i--) {
    digitalWrite(led[i], 1);
    delay(paus);
    digitalWrite(led[i], 0);
    delay(paus / 10);
  }
```

####  Aufgabe 2 
#####  Start über Taster 

```c
int led [7] = {32, 33, 25, 26, 27, 14, 12};
int paus = 200;
int taster = 19;

void setup() {
  for (int i = 0; i < 7; i++) {
    pinMode(led[i], OUTPUT);
  }
  pinMode(taster, INPUT_PULLUP);
  Serial.begin(115200);
  delay(200);
  Serial.print("Warte auf Anweisungen");
  while (digitalRead(taster) == 1) {
    Serial.print(".");
    delay(500);
  }

}

void loop() {
  for (int i = 0; i < 7; i++) {
    digitalWrite(led[i], 1);
    delay(paus);
    digitalWrite(led[i], 0);
    delay(paus / 10);
  }
  for (int i = 5; i >= 0; i--) {
    digitalWrite(led[i], 1);
    delay(paus);
    digitalWrite(led[i], 0);
    delay(paus / 10);
  }

}
```

#####  Richtung über Taster 

```c
int led [7] = {32, 33, 25, 26, 27, 14, 12};
int paus = 200;
int taster = 19;
int zaehler = 0;

void setup() {
  for (int i = 0; i < 7; i++) {
    pinMode(led[i], OUTPUT);
  }
  pinMode(taster, INPUT_PULLUP);
  Serial.begin(115200);
  delay(200);
  Serial.print("Warte auf Anweisungen");
  while (digitalRead(taster) == 1) {
    Serial.print(".");
    delay(500);
  }
}

void loop() {
  if (digitalRead(taster) == 1) {
    for (int i = 0; i < 7; i++) {
      digitalWrite(led[i], 1);
      delay(paus);
      digitalWrite(led[i], 0);
      delay(paus / 10);
    }
  }
  else {
    zaehler ++;
    Serial.println("");
    Serial.print(zaehler);  
    for (int i = 5; i >= 0; i--) {
      digitalWrite(led[i], 1);
      delay(paus);
      digitalWrite(led[i], 0);
      delay(paus / 10);
    }
  }
}
```

####  Aufgabe 3 
#####  Ausgabe über LCD 

```c
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x27, 20, 4);

int led [7] = {32, 33, 25, 26, 27, 14, 12};
int paus = 200;
int taster = 19;
int zaehler = 0;

void setup() {
  lcd.init();
  lcd.backlight();
  for (int i = 0; i < 7; i++) {
    pinMode(led[i], OUTPUT);
  }
  pinMode(taster, INPUT_PULLUP);
  Serial.begin(115200);
  delay(200);
  Serial.print("Warte auf Anweisungen");
  while (digitalRead(taster) == 1) {
    Serial.print(".");
    delay(500);
  }
}

void loop() {
  lcd.setCursor(0, 0); 
  lcd.print("Anzahl"); 
  lcd.setCursor(0, 1); 
  lcd.print("der Durchlaeufe");
  lcd.setCursor(0, 2);
  lcd.print("rueckwaerts:");
  lcd.setCursor(0, 3);
  lcd.print(zaehler);
  if (digitalRead(taster) == 1) {
    for (int i = 0; i < 7; i++) {
      digitalWrite(led[i], 1);
      delay(paus);
      digitalWrite(led[i], 0);
      delay(paus / 10);
    }
  }
  else {
    zaehler ++;
    Serial.println("");
    Serial.print(zaehler);
    for (int i = 5; i >= 0; i--) {
      digitalWrite(led[i], 1);
      delay(paus);
      digitalWrite(led[i], 0);
      delay(paus / 10);
    }
  }
}
```

####  Aufgabe 4 
#####  Funktion für die Ansteuerung der LEDs 

```c
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x27, 20, 4);

int led [7] = {32, 33, 25, 26, 27, 14, 12};
int paus = 200;
int taster = 19;
int zaehler = 0;

void setup() {
  lcd.init();
  lcd.backlight();
  for (int i = 0; i < 7; i++) {
    pinMode(led[i], OUTPUT);
  }
  pinMode(taster, INPUT_PULLUP);
  Serial.begin(115200);
  delay(200);
  Serial.print("Warte auf Anweisungen");
  while (digitalRead(taster) == 1) {
    Serial.print(".");
    delay(500);
  }
}

void loop() {
  lcd.setCursor(0, 0);
  lcd.print("Anzahl");
  lcd.setCursor(0, 1);
  lcd.print("der Durchlaeufe");
  lcd.setCursor(0, 2);
  lcd.print("rueckwaerts:");
  lcd.setCursor(0, 3);
  lcd.print(zaehler);
  lauflicht();
}

void lauflicht() {
  if (digitalRead(taster) == 1) {
    for (int i = 0; i < 7; i++) {
      digitalWrite(led[i], 1);
      delay(paus);
      digitalWrite(led[i], 0);
      delay(paus / 10);
    }
  }
  else {
    zaehler ++;
    Serial.println("");
    Serial.print(zaehler);
    for (int i = 5; i >= 0; i--) {
      digitalWrite(led[i], 1);
      delay(paus);
      digitalWrite(led[i], 0);
      delay(paus / 10);
    }
  }
}
```

#####  Funktion für Darstellung auf LCD 

```c
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x27, 20, 4);

int led [7] = {32, 33, 25, 26, 27, 14, 12};
int paus = 200;
int taster = 19;
int zaehler = 0;

void setup() {
  lcd.init();
  lcd.backlight();
  for (int i = 0; i < 7; i++) {
    pinMode(led[i], OUTPUT);
  }
  pinMode(taster, INPUT_PULLUP);
  Serial.begin(115200);
  delay(200);
  Serial.print("Warte auf Anweisungen");
  while (digitalRead(taster) == 1) {
    Serial.print(".");
    delay(500);
  }
}

void loop() {
  LCD("Anzahl", "der Durchlaeufe", "rueckwaerts", zaehler);
  lauflicht();
}

void LCD(String zeile1, String zeile2, String zeile3, int zeile4) {
  lcd.setCursor(0, 0);
  lcd.print(zeile1);
  lcd.setCursor(0, 1);
  lcd.print(zeile2);
  lcd.setCursor(0, 2);
  lcd.print(zeile3);
  lcd.setCursor(0, 3);
  lcd.print(zeile4);
}

void lauflicht() {
  if (digitalRead(taster) == 1) {
    for (int i = 0; i < 7; i++) {
      digitalWrite(led[i], 1);
      delay(paus);
      digitalWrite(led[i], 0);
      delay(paus / 10);
    }
  }
  else {
    zaehler ++;
    Serial.println("");
    Serial.print(zaehler);
    for (int i = 5; i >= 0; i--) {
      digitalWrite(led[i], 1);
      delay(paus);
      digitalWrite(led[i], 0);
      delay(paus / 10);
    }
  }
}
```

#####  Funktion für Zähler 

```c
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x27, 20, 4);

int led [7] = {32, 33, 25, 26, 27, 14, 12};
int paus = 200;
int taster = 19;
int zaehler = 0;

void setup() {
  lcd.init();
  lcd.backlight();
  for (int i = 0; i < 7; i++) {
    pinMode(led[i], OUTPUT);
  }
  pinMode(taster, INPUT_PULLUP);
  Serial.begin(115200);
  delay(200);
  Serial.print("Warte auf Anweisungen");
  while (digitalRead(taster) == 1) {
    Serial.print(".");
    delay(500);
  }
}

void loop() {
  LCD("Anzahl", "der Durchlaeufe", "rueckwaerts", zaehler);
  lauflicht();
}

int zahler() {
  zaehler ++;
  Serial.println("");
  //Serial.print(zaehler);
  return zaehler;
}

void LCD(String zeile1, String zeile2, String zeile3, int zeile4) {
  lcd.setCursor(0, 0);
  lcd.print(zeile1);
  lcd.setCursor(0, 1);
  lcd.print(zeile2);
  lcd.setCursor(0, 2);
  lcd.print(zeile3);
  lcd.setCursor(0, 3);
  lcd.print(zeile4);
}

void lauflicht() {
  if (digitalRead(taster) == 1) {
    for (int i = 0; i < 7; i++) {
      digitalWrite(led[i], 1);
      delay(paus);
      digitalWrite(led[i], 0);
      delay(paus / 10);
    }
  }
  else {
    Serial.print(zahler());
    for (int i = 5; i >= 0; i--) {
      digitalWrite(led[i], 1);
      delay(paus);
      digitalWrite(led[i], 0);
      delay(paus / 10);
    }
  }
}
```