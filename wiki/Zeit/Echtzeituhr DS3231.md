# Echtzeituhr

###  Zielsetzung 
Bei allen Messreihen ist es interessant wann die einzelnen Datenpunkte gemessen wurden. Die interne Zeitmessung ist für längerfristige Messungen zu ungenau. Zudem fängt diese bei jedem Neustart/Stromverlust wieder neu an zu zählen.

Das DS3231 RTC Modul ist eine einfache Möglichkeit Zeitangaben zu erhalten.
###  Library 
RTCLib by Adafruit

Dokumentation:
[[https://adafruit.github.io/RTClib/html/index.html]]
###  Verkabelung 
|Mikrocontroller | DS3231 |
|---|---|
|Pin 21 | SDA |
|Pin 22 | SCL |
|3,3V | Vin |
|GND | GND |

![[esp32-ds3231_steckplatine.png]]
###  Programmierung 

####  minimales Beispiel 
Ein minimales funktionierendes Beispiel wie Datum, Uhrzeit und Temperatur abgefragt werden können.

```c
// die Adafruit RTC Library einbinden
#include "RTClib.h"

// ein rtc Object erstellen
RTC_DS3231 rtc; 

void setup () 
{
Serial.begin(57600);

while (! rtc.begin()) {
    Serial.println("Konnte RTC nicht finden!");
    delay(500);
  }
// in der nächsten Zeile wird der RTC mit dem Zeitpunkt des Kompilierens gestellt
rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
}

void loop()
{

// es wird ein Zeitpunkt Objekt geholt in dem die akutellen Infos stecken
    DateTime now = rtc.now(); 

    Serial.print(now.day(), DEC);
    Serial.print(".");
    Serial.print(now.month(), DEC);
    Serial.print(".");
    Serial.println(now.year(), DEC);

    Serial.print(now.hour(), DEC);
    Serial.print(":");
    Serial.print(now.minute(), DEC);
    Serial.print(":");
    Serial.println(now.second(), DEC);
    Serial.println(rtc.getTemperature());
    Serial.println();
    delay(3000);
}
```

####  die Uhrzeit einstellen 
Mit dem folgenden Befehl wird die aktuelle Systemzeit im RTC eingestellt.

```c
rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
```

An die Funktion adjust wird ein DateTime Objekt übergeben, in welchem die neue Zeit steht.
Der Konstruktor von DateTime kann auch mit 6 Integerwerten aufgerufen werden, falls man eine andere Zeit einstellen möchte.

```c
int jahr = 2022;
int monat = 1;
int tag = 15;
int stunde = 8;
int minute = 0;
int sekunde = 0;
rtc.adjust(DateTime(jahr,monat,tag,stunde,minute,sekunde));
```

Der Konstruktor von DateTime kann auch mit der UnixZeit aufgerufen werden. Die UnixZeit sind die Sekunden die seit dem 1.1.1970 um 0:00 vergangen sind.

```c
uint32_t unixT = 1643810539;
rtc.adjust(DateTime(t));
```

###  die Unixtime erhalten 

```c
now.unixtime();
```