# ntp Server

###  Zielsetzung 
Da der ESP32 WLAN fähig ist, liegt es nahe die Uhrzeit über das Internet abzufragen.

###  benötigte Libraries 

```c
#include <WiFi.h>
#include "time.h"
```

###  Die Abeichung zu UTC in Sekunden festlegen 

```c
const long  gmtOffset_sec = 3600;
```

###  gesamtes Programm 

```c
#include <WiFi.h>
#include "time.h"

const char* ssid       = "SSID";       // muss angepasst werden
const char* password   = "passwort";   // muss angepasst werden

const char* ntpServer = "pool.ntp.org";
const long  gmtOffset_sec = 3600;
const int   daylightOffset_sec = 3600;

void printLocalTime()
{
  struct tm timeinfo;
  if(!getLocalTime(&timeinfo)){
    Serial.println("Failed to obtain time");
    return;
  }
  Serial.println(&timeinfo, "%A, %B %d %Y %H:%M:%S");
}

void setup()
{
  Serial.begin(115200);

  //connect to WiFi
  Serial.printf("Connecting to %s ", ssid);
  WiFi.begin(ssid, password);                   // hier wird die WLAN Verbindung gestartet
  while (WiFi.status() != WL_CONNECTED) {       // warten auf die Verbindung
      delay(500);
      Serial.print(".");
  }
  Serial.println(" CONNECTED");

  //init and get the time
  configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);
  printLocalTime();

  //disconnect WiFi as it's no longer needed
  WiFi.disconnect(true);
  WiFi.mode(WIFI_OFF);
}

void loop()
{
  delay(1000);
  printLocalTime();
}
```