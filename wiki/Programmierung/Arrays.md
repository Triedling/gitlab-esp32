# Arrays

Als Array bezeichnet man in C eine Datenstruktur in der viele gleichartige Daten gespeichert werden kann. Die Länge eines Arrays wird bei der Erzeugung festgelegt und kann auch nicht verändert werden!

###  Deklaration (Erzeugung) 

In C kann ein Array auf folgende Arten deklariert werden:

```c
int meinArray[6];
int mein_anderes_Array[]={1,4,122,3,0};
```
Im zweiten Fall wurde das Array nicht nur deklariert, sondern auch gleich initialisiert.

###  Abfrage eines Arrays 

```c
x = meinArray[3];
```

Es wird der 4. Eintrag des Arrays in die Variable x geschrieben. In der Informatik beginnt man bei 0 mit dem Zählen. 

###  Wertzuweisung 

```c
meinArray[2]= 5;
```
An die dritte Stelle des Arrays wird die Zahl 5 geschrieben.