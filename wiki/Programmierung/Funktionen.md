# Funktionen

Funktionen manchmal auch Methoden genannt, sind dafür da einzelne Teile eines Programms, welches immer wieder gebraucht wird zusammenzufassen und mit einem Namen zu versehen.

##  Funktionen definieren 

```c
Rückgabetyp Funktionsname(Datentyp_Parameter1 Name_Parameter1, Datentyp_Parameter2 Name_Parameter2,...){

   Befehle;

   return Ergebnis;
}
```

##  Funktionen aufrufen 

```c
Variable = Funktionsname (Wert1, Wert2,...);
```

##  Funktionen ohne Parameter und ohne Rückgabewert 
###  Funktion definieren 

```c
void leuchte1Sekunde(){
   digitalWrite(5,HIGH);
   delay(1000);
   digitalWrite(5,LOW);
   delay(1000);
}
```

###  Funktion aufrufen 

```c
leuchte1Sekunde();
```

##  Funktionen mit Parameter und ohne Rückgabewert 
###  Funktion definieren 

```c
void leuchte_X_Sekunden(int x){
   digitalWrite(5,HIGH);
   delay(x*1000);
   digitalWrite(5,LOW);
   delay(1000);
}
```

###  Funktion aufrufen 

```c
leuchte_X_Sekunden(3);
```

##  Funktionen ohne Parameter und mit Rückgabewert 
###  Funktion definieren 

```c
float getTemperatur(){

   ...

   return temp;
}
```

###  Funktion aufrufen 

```c
float t;
...
t = getTemperatur();
```

##  Funktionen mit Parameter und mit Rückgabewert 

###  Funktion definieren 

```c
int addiere(int a, int b){
   return a + b;
}
```

##  Funktion aufrufen 

```c
x = addiere(5,7);
```