# Blinken ohne delay
Möchte man etwas regelmäßig ablaufen lassen, dann wird bei Anfängern meist ein delay benutzt. Dies hat den Nachteil, dass der MC während des delays blockiert ist und nichts anderes machen kann. Eine Alternative ist die Abfrage der seit dem Start vergangenen Millisekunden. Dies geschieht mit dem Befehl millis() .

##  Beispiel Programm 

```c
long letzteZeit = 0;
int dauer = 0;
int led = 36;
bool status = 0;

void setup(){

   pinMode(led,OUTPUT);

}

void loop(){

   if (millis() - letzteZeit > dauer){
     digitalWrite(led,status);
     status != status;
     letzteZeit = millis();
   }

}
```