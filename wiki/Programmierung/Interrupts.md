# Interrupts
##  Das Problem 

Während ein Lauflicht läuft, soll parallel dazu das drücken eines Tasters gezählt und auf dem Serielen Monitor ausgegeben werden. 
###  Schaltung 

###  Programmierung Erste Idee 

```c
int[] leds = {32,33,25}

taster = 5;

counter = 0;

void setup(){

   for(int i = 0; i <4; i++){
      pinMode(leds[i],OUTPUT);
   } 
   pinMode(taster,INPUT);  
}

void loop(){
   if (digitalRead(taster)==HIGH) counter++;
   Serial.println(counter);

   for(int i = 0; i <4; i++){

      digitalWrite(leds[i],HIGH);
      delay(500);
      digitalWrite(leds[i],LOW);
   }

   for(int i = 4; i >0; i--){

      digitalWrite(leds[i],HIGH);
      delay(500);
      digitalWrite(leds[i],LOW);
   }
}
```

Was sind die Probleme?

##  Lösung: Interrupts 
###  Arten von Interrupts 
* Hardware Interrupts: reagieren auf eine Zustandsänderung eines Pins
* Software Interrupts: reagieren z.Bsp. auf einen Timer

Für uns sind hier nur die Hardware Interrupts interessant.

###  Hardware Interrupts 

Alle GPIO Pins können beim ESP32 für Interrupts genutzt werden. Aber wenn man einen Pin benutzt, der auch für serielle Kommunikation benutzt wird, kann es dazu führen, dass Flanken gezählt werden, die durch die Kommunikation entstehen.

####  Einem Interrupt einen Pin zuweisen 

```c
attachInterrupt(GPIOPin, ISR, Mode);
```

#####  GPIOPin
Gibt an an welchem Pin auf den Interrupt gewartet wird.

#####  ISR (Interrupt Service Routine) 
Sagt welche Funktion aufgerufen wird, sobald der Interrupt ausgelöst wurde.

#####  Mode 
Gibt die Art des Interrupts an.

* LOW
* HIGH
* CHANGE
* FALLING
* RISING

####  Ein Interrupt wieder von einem Pin "lösen" 

```c
detachInterrupt(GPIOPin);
```

####  Eine ISR definieren 

```c
void IRAM_ATTR name_von_isr (){
   // hier wird irgendetwas gemacht
}
```

#####  IRAM_ATTR 
Bedeutet die Funktion wird nicht im Flash gespeichert sondern im internen Speicher des ESP32, auf den viel schneller zugegriffen werden kann. Dies ist nicht vorgeschrieben, aber sehr sinnvoll.

#####  kein return 
Eine ISR sollte möglichst nichts zurückgeben, sondern einfach nur kurz den Zustand einer Variablen ändern.

#####  keine Parameter! 
Eine ISR kann keine Parameter beinhalten!

#####  so kurz wie möglich 
Die ISR sollte so kurz wie möglich sein, da sie mitten im Ablauf das Programm unterbricht.

####  Programm mit Interrupts 

```c
int leds[] = {32,33,25};

int taster = 5;

int counter = 0;

long zeit = 0;

void IRAM_ATTR zaehlen(){

  if (millis() - zeit>250){
    counter++;
    zeit = millis();
  }

}

void setup(){
  Serial.begin(115200);
   attachInterrupt(taster, zaehlen, RISING);
   for(int i = 0; i <3; i++){
      pinMode(leds[i],OUTPUT);
   }   
}

void loop(){

   Serial.println(counter);

   for(int i = 0; i <3; i++){

      digitalWrite(leds[i],HIGH);
      delay(500);
      digitalWrite(leds[i],LOW);
   }

   for(int i = 2; i >=0; i--){

      digitalWrite(leds[i],HIGH);
      delay(500);
      digitalWrite(leds[i],LOW);
   }
}
```

## Alternatives Beispiel mit NeoPixel
```c 
#include <Adafruit_NeoPixel.h>

int taster = 5;
int counter = 0;
long zeit = 0;

int NUM_PIXELS = 16;
int PIN_NEO_PIXEL = 32;

Adafruit_NeoPixel NeoPixel(NUM_PIXELS, PIN_NEO_PIXEL, NEO_GRB + NEO_KHZ800);

void IRAM_ATTR zaehlen(){

	if (millis() - zeit>250){

		counter++;
		zeit = millis();
	}
}

void setup(){

	Serial.begin(115200);
	NeoPixel.begin();
	attachInterrupt(taster, zaehlen, RISING);
}

void loop(){

	Serial.println(counter);  
	for(int i = 0; i <16; i++){

		NeoPixel.clear();
		NeoPixel.setPixelColor(i, NeoPixel.Color(0, 255, 0));
		delay(500);
		NeoPixel.show();
	}
} 
```