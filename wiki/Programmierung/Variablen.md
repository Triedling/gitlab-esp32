# Variablen
###  Variablen deklarieren 

```c
int a;
```

###  Variablen initialisieren 

```c
a = 5;
```

###  Variablen deklarieren und initialisieren 

```c
int a = 5;
```

###  Sichtbarkeit von Variablen  
Je nachdem in welchem Bereich man Variablen deklariert, sind sie auch benutzbar/sichtbar.

####  global 

Werden sie außerhalb der Funktionen definiert dann sind es globale Variablen und sind überall sichtbar.

####  lokal 

Werden sie innerhalb von geschweiften Klammern deklariert. Dh. innerhalb von Funktionen, Schleifen, Verzweigungen, ..., dann sind sie nur innerhalb dieser Klammern sichtbar/benutzbar.

####  Konkurrierende Deklaration 
Wird an verschiedenen Stellen eine Variable mit dem gleichen Namen deklariert, dann sind das für den Computer verschiedene Variablen. 
Beim Zugriff haben lokaleren Variablen Vorrang.

**Aufgabe:**
Finde heraus welche Werte a hat an den drei verschiedenen Stellen.

```c
int a = 5;

void setup(){
   // Stelle 1
   int a = 9;
   // Stelle 2
   for (int a = 0; a<12; a++){
   // Stelle 3
   }
}
```