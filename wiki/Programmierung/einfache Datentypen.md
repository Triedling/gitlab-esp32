# Datentypen

| C++ Datentyp   | Min. Wert           | Max. Wert      | Speicherbedarf                           | Beispiel                                            |
| -------------- | ------------------- | -------------- | ---------------------------------------- | --------------------------------------------------- |
| bool (Boolean) | 0 (false),1 (true)  | 1 Byte = 8 Bit | An/Ausschalten von Features wie Darkmode |                                                     |
| byte           | 0                   | 255            | 1 byte                                   | Alter (Wobei dies nicht Zukunftssicher sein könnte) |
| char           | ASCII char von -127 | Bis 128        | 1 byte                                   | Namen der Wochentage                                |
| int            | -2,147,483,648      | 2,147,483,647  | 2 bytes                                  | In Schleifen                                        |
| short          | -32768              | 32767          | 2 bytes                                  | Längenangabe in M (wenn Genauigkeit ausreicht)      |
| long           | 0                   | 4294967295     | 4 bytes<Bei großen Schleifen             |                                                     |
| float          | -3.4028235E+38      | 3.4028235E+38  | 4 bytes                                  | Temperatur                                          |
| double         | -3.4028235E+38      | 3.4028235E+38  | 8 bytes                                  | NASA-Präzisionsberechnung                           |