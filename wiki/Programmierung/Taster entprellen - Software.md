# Taster entprellen - Softwarelösung

```c 
int taster = 17; 
int counter = 0; 
long zeit = 0; 

void Taster_zaehlen(){ 
	if (millis() - zeit>250){ 
		counter++; 
		zeit = millis(); 
	} 
} 

void setup(){

	Serial.begin(115000);
}

void loop(){
	Taster_zaehlen();
	Serial.println(counter);
}
```
