# for-Schleife
```c 
for (int i = 0; i<10;i=i+1){
	digitalWrite(i, HIGH);
	delay(1000);
	digitalWrite(i, LOW);
	delay(1000);
} 
```
```c 
int i = 0 
```
Deklaration und Initialisierung einer Zählvariablen.
```c 
i<10 
```
Bedingung, so lange diese true ist wird die Schleife durchgeführt.

```c 
i=i+1 
```
Veränderung der Zählvariablen